/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.tonetag.desktop.seller.bean.Merchant;
import com.tonetag.desktop.seller.bean.Settings;
import com.tonetag.desktop.seller.bean.Teller;
import com.tonetag.desktop.seller.constants.Applib;
import com.tonetag.desktop.seller.constants.Constants;
import com.tonetag.desktop.seller.frames.DialogFrame;
import com.tonetag.desktop.seller.panels.DesktopSettingsPanel;
import com.tonetag.desktop.seller.panels.DominosTransactionPanel;
import com.tonetag.desktop.seller.panels.RegisterPanel;
import com.tonetag.desktop.seller.panels.TransactionHistoryPanel;
import com.tonetag.desktop.seller.panels.TransactionPanel;
import com.tonetag.desktop.seller.tasks.JsonParser;
import com.tonetag.desktop.seller.tasks.NetworkAsyncTask;
import com.tonetag.desktop.seller.tasks.ServiceHandler;
import com.tonetag.desktop.seller.tasks.Utils;
import com.tonetag.dll.DllClient;

import jublfood.payment.api.OrderDetails;
import jublfood.payment.api.PaymentProcessing;

/**
 *
 * @author dev1
 */
public class Seller extends javax.swing.JFrame {
    public String amount;
    public String orderID;
    public String phone;
    public String storeNumber;
    public String terminalNumber;
    public String tnsMod;
    public String tnsFlowMode;
    public String refundAmount;
    public String cancelAmount;
    public String track_Ref_ID;
    private String TRANS_MOD = null;
    private final String TRANS = "TRANS";
    private final String REFUND = "REFUND";
    private final String CANCEL = "CANCEL";
    static {
        if(Utils.isUnix()){
            if(Utils.isX86()){
                System.loadLibrary("ToneTagLX86");
            }
            if(Utils.isX64()){
                System.loadLibrary("ToneTagLX64");
            }
        }
        
        if(Utils.isWindows()){
//            System.loadLibrary("ToneTagWin");
//        	new Applib().loadLib();
        	System.load(new File(Constants.TONETAG_DLL).getAbsolutePath());
            //System.load("C:\\tonetag\\lib\\libToneTagWin.dll");
        }
        if(Utils.isMac()){
            System.loadLibrary("ToneTagMac");
        }
    }

	 public Seller(){
		 
	 }
	 private NetworkAsyncTask mNetworkAsyncTask;
	 
    /**
     * Creates new form Seller
     */
	public Seller(String args[]) {

/*		if (args.length == 1) {
			this.orderID = args[0];
			this.TRANS_MOD = TRANS;
		} else if (args.length == 2) {
			this.orderID = args[0];
			if(args[1].equalsIgnoreCase(REFUND))
				this.TRANS_MOD = REFUND;
			else if(args[1].equalsIgnoreCase(CANCEL))
				this.TRANS_MOD = CANCEL;
		} else if (args.length == 3) {
			this.orderID = args[0];
			this.tnsMod = args[1];
			this.tnsFlowMode = args[2];
		} else {
			JOptionPane.showMessageDialog(this,
					"please send OrderId and Mode of Transaction in order to make ToneTag Transaction");
			System.exit(0);
		}
*/
		
		if (args.length == 2) {
			this.orderID = args[0];
			this.tnsMod = args[1];
		} else if (args.length == 3) {
			this.orderID = args[0];
			this.tnsMod = args[1];
			this.tnsFlowMode = args[2];
		} else {
			JOptionPane.showMessageDialog(this,
					"please send OrderId and Mode of Transaction in order to make ToneTag Transaction");
			System.exit(0);
		}
		
		initComponents();
		String receivedData = getFileData(Constants.SETTINGS_FILE_NAME);
		if ((receivedData == null) || (receivedData.isEmpty())) {
			setSettingsPanel();
			setRegisterPanel();
		}
		
		// load settong object for traversal
		initSettings(receivedData);
		receivedData = getFileData(Constants.SELLER_FILE_NAME);
		if ((receivedData == null) || (receivedData.isEmpty())) {
			setRegisterPanel();
		} else {
			initMerchantDetails(receivedData);
			callTransactionFunction(tnsMod);
		}
		
		pack();
	}
	
	public void callTransactionFunction(String mod){
		setOrderDetails(mod);
		switch (mod) {
		case "1":
			if(tnsFlowMode != null && !tnsFlowMode.isEmpty() && tnsFlowMode.equals("1"))
				setDominosTransactionPanel();
			else
				setTransactionPanel();
			break;
		case "2":
			refundOrCancelProcess(orderID);
			break;
		case "3":
			refundOrCancelProcess(orderID);
			break;
		case "4":
			displayStatusProcess(orderID);
			break;
		default:
			JOptionPane.showMessageDialog(this,"Transaction mode should be 1 to 4 only.");
			System.exit(0);
			break;
		}
	}

	void setOrderDetails(String mod){
		PaymentProcessing pp = new PaymentProcessing(Constants.CONFIG_FILE_NAME);
		OrderDetails od = null;
		switch (mod) {
		case "1":
			od = pp.GetOrderDetails(this.orderID);
			break;
		case "2":
			od = pp.GetOrderDetailRefund(this.orderID);
			break;
		case "3":
			od = pp.GetOrderDetailCancel(this.orderID);
			break;
		case "4":
			od = pp.GetOrderDetails(this.orderID);
			break;
		default:
			JOptionPane.showMessageDialog(this,"Transaction mode should be 1 to 4 only.");
			System.exit(0);
			break;
		}
		if(od != null){
			String storeNumber = od.getLocationCode();
			String phone = od.getPhoneNumber();
			String terminalNumber = od.getComputerName();
			String orderID = od.getTransactionID();
			String amount = getFormatedAmount(od.getOrderAmount());
			String refundAmount = getFormatedAmount(od.getRefundAmount());
			String cancelAmount = getFormatedAmount(od.getCancelAmount());
			
			/*System.out.println("comptr name: "+od.getComputerName());
			System.out.println("odr amount: "+amount);
			System.out.println("ref amount: "+refundAmount);
			System.out.println("cancel amount: "+cancelAmount);
			System.out.println("name: "+od.getName());
			System.out.println("odr number: "+od.getOrderNumber());
			System.out.println("odr trak id: "+od.getOrderTakerID());
			System.out.println("order Id: "+od.getTransactionID());
			System.out.println("work stn id: "+od.getWorkstationID());*/
			
			String ordrId = od.getTransactionID();
			if(ordrId == null || ordrId.isEmpty()){
				JOptionPane.showMessageDialog(this, this.orderID+" Order Id not found.");				
				System.exit(0);
			}
			
			if(storeNumber != null && !storeNumber.isEmpty())
				this.storeNumber = storeNumber;
			
			if(phone != null && !phone.isEmpty())
				this.phone = phone;
			
			if(terminalNumber != null && !terminalNumber.isEmpty())
				this.terminalNumber = od.getComputerName();
			
			if(orderID != null && !orderID.isEmpty())
				this.orderID = orderID;
			
			if(amount != null && !amount.isEmpty())
				this.amount = getFormatedAmount(amount);
			
			if(refundAmount != null && !refundAmount.isEmpty())
				this.refundAmount = getFormatedAmount(refundAmount);
			
			if(cancelAmount != null && !cancelAmount.isEmpty())
				this.cancelAmount = getFormatedAmount(cancelAmount);
		} else {
			JOptionPane.showMessageDialog(this,
					"given order id not found, please try again with proper order id");
			System.exit(0);
		}
	}
	
	private void displayStatusProcess(String orderId) {
		if (storeNumber == null || storeNumber.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Location not found for "+orderID+" order id, please try again with proper order id");
			System.exit(0);
		}
		Applib.call_StatusAPI(orderId, storeNumber);
	}

	private void refundOrCancelProcess(String orderId) {
		try {
			if(storeNumber == null || storeNumber.isEmpty()){
				JOptionPane.showMessageDialog(this, "location not found for "+orderID+" order id, please try again with proper order id");				
				System.exit(0);
			}
			if(phone == null || phone.isEmpty()){
				JOptionPane.showMessageDialog(this, "phone number not found for "+orderID+" order id, please try again with proper order id");				
				System.exit(0);
			}
			if(tnsMod.equals("2")){
				if(refundAmount == null || refundAmount.isEmpty()){
					JOptionPane.showMessageDialog(this, "refund amount not found for "+orderID+" order id, please try again with proper order id");
					System.exit(0);
				}
			} else if(tnsMod.equals("3"))
				if(cancelAmount == null || cancelAmount.isEmpty()){
					JOptionPane.showMessageDialog(this, "cancel amount not found for "+orderID+" order id, please try again with proper order id");
					System.exit(0);
				}
			
			Map<String, Object> params = new HashMap<>();
			params.put("location_code", storeNumber);
//			params.put("comp_name", terminalNumber);
			params.put("order_id", orderID);
			params.put("phone", phone);
			if(tnsMod.equals("2"))
				params.put("refund_amount", this.refundAmount);
			else if(tnsMod.equals("3"))
				params.put("refund_amount", this.cancelAmount);
			
			String responseMessage = Applib.call_RefundOrCancelApi(params);
			
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(responseMessage.toString());
			String isSuccess = json.get("success").toString();
			if (isSuccess.equalsIgnoreCase("true")) {
				if(tnsMod.equals("2"))
					JOptionPane.showMessageDialog(new Seller(), "refund done successful");
				else if(tnsMod.equals("3"))
					JOptionPane.showMessageDialog(new Seller(), "cancel done successful");
			} else if (isSuccess.equalsIgnoreCase("false")) {
				String mesg = json.get("message").toString() + "";
				if (mesg != null && !mesg.isEmpty())
					JOptionPane.showMessageDialog(new Seller(), mesg);
			}

			System.exit(0);	
		} catch (Exception e) {
			System.exit(0);
		}
	}
	
	public void initMerchantDetails(String response){
        
        //String response = getFileData("seller.dat");

        //System.out.println("file data : " + response);

        JsonParser jp = new JsonParser(response);//seller.sellerFile);
        if(jp.isHavingSuccess()){
            merchant = jp.parseToMerchant();
            tellers = merchant.getTellers();
        }
    }
    
    public void initSettings(String response){
        JsonParser jp = new JsonParser(response);
        mSettings = jp.parseToSetting();
    }

    /*private void showAmountPanel(){
     javax.swing.GroupLayout jpAmountPanelLayout = new javax.swing.GroupLayout(jpAmountPanel);
     jpAmountPanel.setLayout(jpAmountPanelLayout);
     jpAmountPanelLayout.setHorizontalGroup(
     jpBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGap(0, 600, Short.MAX_VALUE)
     );
     jpAmountPanelLayout.setVerticalGroup(
     jpBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGap(0, 379, Short.MAX_VALUE)
     );

     }*/
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem3 = new javax.swing.JMenuItem();
        jpBasePanel = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        jmiTransactionHistory = new javax.swing.JMenuItem();
        jmiSettings = new javax.swing.JMenuItem();
        jmiExit = new javax.swing.JMenuItem();

        jMenuItem3.setText("jMenuItem3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
			public void windowClosing(WindowEvent e) {
            	windowClose();
            }
        });
        setTitle("ToneTag Seller");
        setName("sellerFrame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(700, 700));
        setResizable(false);

        jpBasePanel.setLayout(new java.awt.CardLayout());

        menuBar.setPreferredSize(new java.awt.Dimension(35, 50));

        fileMenu.setText("File");

        jmiTransactionHistory.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jmiTransactionHistory.setText("Transaction History");
        jmiTransactionHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiTransactionHistoryActionPerformed(evt);
            }
        });
        fileMenu.add(jmiTransactionHistory);

        jmiSettings.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        //jmiSettings.setText("Settings");
        jmiSettings.setText("Merchant Registration");
        jmiSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSettingsActionPerformed(evt);
            }
        });
        fileMenu.add(jmiSettings);

        jmiExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jmiExit.setText("Exit");
        jmiExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiExitActionPerformed(evt);
            }
        });
        fileMenu.add(jmiExit);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jpBasePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 601, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        /*jpAmountPanel = new AmountPanel(this);
        jpBasePanel.removeAll();
        jpBasePanel.add(jpAmountPanel);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();*/

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    
    private void jmiSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSettingsActionPerformed
        // TODO open transaction history panel
        // Delete settings file and invoke settings panel
        File file = new File(Constants.SETTINGS_FILE_NAME);
        if(file.exists()){
            file.delete();
        }
        setRegisterPanel();
        //setSettingsPanel();
        
    }//GEN-LAST:event_jmiSettingsActionPerformed

    private void jmiTransactionHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiTransactionHistoryActionPerformed
        // TODO add your handling code here:
        setTransactionHistoryPanel();
    }//GEN-LAST:event_jmiTransactionHistoryActionPerformed

    private void jmiExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiExitActionPerformed
        // TODO add your handling code here:
    	windowClose();
    }//GEN-LAST:event_jmiExitActionPerformed
    
    private void windowClose(){
    	if(track_Ref_ID != null && !track_Ref_ID.isEmpty()){
    		int confirm = JOptionPane.showOptionDialog(
                    new Seller(), "Do you want cancel transaction", "Exit Confirmation", JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE, null, null, null);
               if (confirm == 0) {
            	  callCancelTransProcess(track_Ref_ID);
               }
    	} else{
			int confirm = JOptionPane.showOptionDialog(new Seller(), "Are You Sure to Close ToneTag?",
					"Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
					null);
			if (confirm == 0) {
				System.exit(0);
			}
    	}
    }
    
	private void callCancelTransProcess(String transId) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("transaction_id", transId));
		String ss = new ServiceHandler().makeServiceCall(Constants.API_TRANSACTION_CANCEL, ServiceHandler.POST, params);
		JsonParser jsonParser = new JsonParser((String) ss);
		try {
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(ss.toString());
			if (jsonParser.isHavingSuccess()) {
				String isSuccess = json.get("success").toString();
				System.out.println("json: " + json);
				if (isSuccess.equalsIgnoreCase("true")) {
					if(isEnabled())
						setVisible(false);
					JOptionPane.showMessageDialog(new Seller(), "Transaction hasbeen canceled");
				}
			} else {
				String mesg = json.get("message").toString() + "";
				if (mesg != null && !mesg.isEmpty()) {
					JOptionPane.showMessageDialog(new Seller(), mesg);
				}
			}
		} catch (ParseException e) {
		}
		System.exit(0);
	}
    private void setTransactionPanel(){
    	jpTransaction = new TransactionPanel(this);
    	jpBasePanel.removeAll();
        jpTransaction.setAmount(this.amount);
        jpBasePanel.add(jpTransaction);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    private TransactionPanel jpTransaction;
    private DominosTransactionPanel jpDominosTransaction;
    
    private void setDominosTransactionPanel(){
    	jpDominosTransaction = new DominosTransactionPanel(this);
    	jpBasePanel.removeAll();
    	jpDominosTransaction.setAmount(this.amount);
        jpBasePanel.add(jpDominosTransaction);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }

    
    private void setTransactionHistoryPanel(){
        jpTransactionHistoryPanel = new TransactionHistoryPanel(this);
        jpBasePanel.removeAll();
        jpBasePanel.add(jpTransactionHistoryPanel);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    private void setSettingsPanel(){
        jpSettingsPanel = new DesktopSettingsPanel(this);//SettingsPanel(this);
        jpBasePanel.removeAll();
        //jpBasePanel.add(jpSettingsPanel);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    private void setRegisterPanel(){
        jpRegisterPanel = new RegisterPanel(this);
        jpBasePanel.removeAll();
        jpBasePanel.add(jpRegisterPanel);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
       	try {
    			DllClient.loadDllLibrary();
    			
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	final Seller seller = new Seller(args);
//    	final Seller seller = new Seller("1");
        //seller.sellerFile = new File("/res/seller.json");
        
        

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            //javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                javax.swing.UIManager.setLookAndFeel(info.getClassName());
                break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Seller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Seller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Seller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Seller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                seller.setVisible(true);
            }
        });
    }

    /*private String getFile(String fileName) {

        StringBuilder result = new StringBuilder("");
        
        ClassLoader cl = getClass().getClassLoader();
        InputStream imageURL = cl.getResourceAsStream("resources/seller.json");
        BufferedReader in;  // BufferedReader for reading from standard input.
        in = new BufferedReader( new InputStreamReader( imageURL ) );
        //String line = "";
        try {
           String line = in.readLine();
           while ( line != null ) { 
               result.append(line);
              line = in.readLine();
           }
        }
        catch (IOException e) {
        }

        

        return result.toString();

    }*/
    
    private String getFileData(String fileName) {

        File file = new File(fileName);
        InputStream inStream = null;
        String decryptedText = "";
        try {
            inStream = new FileInputStream(file);
            byte[] buff = new byte[inStream.available()];
            inStream.read(buff);
            decryptedText = Utils.decrypt(buff);
        } catch (Exception ex) {
            //Logger.getLogger(FileManageMent.class.getName()).log(Level.SEVERE, null, ex);
        }

        return decryptedText;

    }
    
    public void startDialog(String message){
        /*progressMonitor = new ProgressMonitor(EnterAmountPanel.this,
                                  "Processing ...",
                                  "", 0, 100);
        progressMonitor.setProgress(0);*/
        this.setEnabled(false);
        mDialogFrame = new DialogFrame(this);
        mDialogFrame.invokeDialog();
        if(this.mOnDialoUpdateListener != null){
            mOnDialoUpdateListener.setText(message);
        }
        //mDialogFrame.setMessage(message);
        
    }
    
    public void setDialogMessage(String message){
        if(this.mOnDialoUpdateListener != null){
            mOnDialoUpdateListener.setText(message);
        }
    }
    
    public void closeDialog(){
        this.setEnabled(true);
        if(mOnDialoUpdateListener != null){
            mOnDialoUpdateListener.closeDialog();//setText("Sending Data to Customer");
        }
    }
    
    public void setOnDialoUpdateListener(OnDialoUpdateListener mOnDialoUpdateListener) {
        this.mOnDialoUpdateListener = mOnDialoUpdateListener;
    }
    
    String getFormatedAmount(String amount){
    	if(amount != null && !amount.isEmpty()){
    		int ind = amount.indexOf(".");
    		if(!amount.contains("."))
    			return amount +".00";
    		int l = amount.substring(amount.indexOf("."),amount.length()).length();
    		if(l == 2)
    			return amount +"0";
    		else
    			return amount.substring(0,ind+3);
    	}
    	return "";
    }
	
    
    public interface OnDialoUpdateListener{
        public void setText(String message);
        
        public void closeDialog();
    }
    
    private OnDialoUpdateListener mOnDialoUpdateListener;
    private DialogFrame mDialogFrame;
    public String selectedTellerCode = null;
    public int selectedTellerCodeIndex = 0;
    public Settings mSettings;

    private javax.swing.JPanel jpAmountPanel;
    private javax.swing.JPanel jpTransactionHistoryPanel;
    private javax.swing.JPanel jpRegisterPanel;
    private javax.swing.JPanel jpSettingsPanel;
    public Merchant merchant;
    public List<Teller> tellers;
    //public Settings settings;
    //private File sellerFile;
    //private Seller seller;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jmiExit;
    private javax.swing.JMenuItem jmiSettings;
    private javax.swing.JMenuItem jmiTransactionHistory;
    public javax.swing.JPanel jpBasePanel;
    private javax.swing.JMenuBar menuBar;
    // End of variables declaration//GEN-END:variables
}
