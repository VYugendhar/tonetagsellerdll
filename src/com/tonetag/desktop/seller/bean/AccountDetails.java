package com.tonetag.desktop.seller.bean;

import java.io.Serializable;

/**
 * Created by Dev1 on 15/06/15.
 */
public class AccountDetails implements Serializable{
    private static final long serialVersionUID = -7406082437623008161L;

    /*private String name;
    private String accType;
    private String accNo;
    private String ifscCode;*/
    private boolean status;

    public AccountDetails(){}

    public AccountDetails(/*String name, String accType, String accNo, String ifscCode,*/ boolean status){
        /*this.name = name;
        this.accType = accType;
        this.accNo = accNo;
        this.ifscCode = ifscCode;*/
        this.status = status;
    }

    /*public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }*/

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /*public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof AccountDetails){
            /*String oAccountType = ((AccountDetails) o).accType;
            if((oAccountType != null) && (this.accType.equals(oAccountType))){
                String oAccountNo = ((AccountDetails) o).accNo;
                if((oAccountNo != null) && (this.accNo.equals(oAccountNo))){
                    String oIfscCode = ((AccountDetails) o).ifscCode;
                    if ((oIfscCode != null)&&(this.ifscCode.equals(oIfscCode))){
                        result = true;
                    }
                }
            }*/
            result = true;
        }
        return result;
    }
}
