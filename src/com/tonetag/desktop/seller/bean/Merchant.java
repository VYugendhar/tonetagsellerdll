package com.tonetag.desktop.seller.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dev1 on 15/06/15.
 */
public class Merchant implements Serializable {
    private static final long serialVersionUID = -7406082437623008161L;

    private String name;
    private String email;
    private String number;
    private String app_id;
    private String kyc_status;
    private String tid;

    private boolean kyc_uploaded;
    private List<AccountDetails> accountDetails;

    private List<Teller>  tellers;

    public Merchant(){

    }

    public Merchant(String name, String email, String number, String app_id,String kyc_status,
                    List<AccountDetails> accountDetails, List<Teller> tellers){
        this.name = name;
        this.email = email;
        this.number = number;
        this.app_id = app_id;
        this.kyc_status = kyc_status;
        this.accountDetails = accountDetails;
        this.tellers = tellers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<AccountDetails> getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(List<AccountDetails> accountDetails) {
        this.accountDetails = accountDetails;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getKyc_status() {
        return kyc_status;
    }

    public void setKyc_status(String kyc_status) {
        this.kyc_status = kyc_status;
    }

    public boolean getKyc_uploaded() {
        return kyc_uploaded;
    }

    public void setKyc_uploaded(boolean kyc_uploaded) {
        this.kyc_uploaded = kyc_uploaded;
    }

    public List<Teller> getTellers() {
        return tellers;
    }

    public void setTellers(List<Teller> tellers) {
        this.tellers = tellers;
    }

    public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	@Override
    public boolean equals(Object o) {
        boolean result = false;
        if(o instanceof Merchant){
            String oName = ((Merchant) o).name;
            if ((oName != null) && (this.name.equals(oName))){
                String oEmail = ((Merchant) o).email;
                if ((oEmail != null) && (this.email.equals(oEmail))){
                    String oNumber = ((Merchant) o).number;
                    if ((oNumber != null) && (this.number.equals(oNumber))){
                        List<AccountDetails> oAccountDetails = ((Merchant) o).accountDetails;
                        if ((oAccountDetails != null) && (this.accountDetails.equals(oAccountDetails))){
                            result = true;
                        }
                    }
                }
            }
        }
        return result;
    }
}
