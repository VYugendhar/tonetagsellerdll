/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller.bean;

/**
 *
 * @author tone
 */
public class Settings {
    
   private String host = "192.168.1.121";//"127.0.0.1";
   private int port = 58627;
   
   private String micMixer = "Device";
   
   private String speakerMixer = "Device";

    public String getMicMixer() {
        return micMixer;
    }

    public void setMicMixer(String micMixer) {
        this.micMixer = micMixer;
    }

    public String getSpeakerMixer() {
        return speakerMixer;
    }

    public void setSpeakerMixer(String speakerMixer) {
        this.speakerMixer = speakerMixer;
    }
   
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
   
   
    
}
