package com.tonetag.desktop.seller.bean;

/**
 * Teller object
 */
public class Teller {

    private String name;
    private String phone;
    private String teller_code;

    public Teller(){

    }

    public Teller(String name, String phone, String teller_code){
        this.name = name;
        this.phone = phone;
        this.teller_code = teller_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTeller_code() {
        return teller_code;
    }

    public void setTeller_code(String teller_code) {
        this.teller_code = teller_code;
    }

    @Override
    public boolean equals(Object o) {

        if (o != null){
            if ((((Teller)o).getName().equals(this.getName())) || (((Teller)o).getPhone().equals(this.getPhone()))){
                    return true;
            }
        }

        return false;
    }
}
