/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller.bean;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Ibrahim
 */
public class TransactionHistoryBean implements ListModel<TransactionLog>{

    @Override
    public int getSize() {
        return this.getSize();
    }

    @Override
    public TransactionLog getElementAt(int index) {
       return this.getElementAt(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
    }
    
}
