package com.tonetag.desktop.seller.bean;

/**
 * Created by customer_support on 07/10/15.
 */
public class TransactionLog {

    String transAmount;
    String transCreatedOn;
    String transId;
    String status;
    double amount;
    
    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }


    public String getTransCreatedOn() {
        return transCreatedOn;
    }

    public void setTransCreatedOn(String transCreatedOn) {
        this.transCreatedOn = transCreatedOn;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
}
