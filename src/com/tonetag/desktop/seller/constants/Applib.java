package com.tonetag.desktop.seller.constants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.tonetag.desktop.seller.Seller;
import com.tonetag.desktop.seller.tasks.JsonParser;
import com.tonetag.desktop.seller.tasks.ServiceHandler;

public class Applib {
	static String USER_AGENT = "Mozilla/5.0";
	private Seller mainForm;
	
	public void loadLib() {
		ClassLoader cl = getClass().getClassLoader();
		String dllPath = cl.getResource(Constants.TONETAG_DLL).getPath();
		int in = dllPath.indexOf("/");
		dllPath = dllPath.substring(in + 1, dllPath.length());
		// System.out.println("dllPath: "+dllPath);
		File l = new File("libToneTagWinx64.dll");
		System.load(l.getAbsolutePath());
	}

	public static String call_RefundOrCancelApi(Map<String, Object> params) throws Exception {

		String responseMessage = null;
		int responseCode = 500;
		String data = "";
		URL obj = new URL(Constants.API_REFUND_AMOUNT);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		String locCode = params.get("location_code").toString();
		// String compNm = params.get("comp_name").toString();
		String ordId = params.get("order_id").toString();
		String phone = params.get("phone").toString();
		String refAmnt = params.get("refund_amount").toString();

		data = "data={" + "\"refund_amount\"" + ":\"" + refAmnt + "\"," + "\"location_code\"" + ":\"" + locCode + "\","
				+ "\"order_id\"" + ":\"" + ordId + "\"," + "\"phone\"" + ":\"" + phone + "\"}";

		// add reuqest header
		con.setRequestMethod("POST");

		con.setRequestProperty("Authorization", Constants.TOKEN);
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(data);
		wr.flush();
		wr.close();

		responseCode = con.getResponseCode();

		StringBuilder response = new StringBuilder();
		if (responseCode == 200) {
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		} else {
			response.append(con.getResponseMessage());
		}
		responseMessage = response.toString();

		return responseMessage;
	}

	public static void call_StatusAPI(String orderId, String location) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("order_id", orderId));
		params.add(new BasicNameValuePair("location_code", location));

		String ss = new ServiceHandler().makeServiceCall(Constants.API_STAUS, ServiceHandler.POST, params);
		JsonParser jsonParser = new JsonParser((String) ss);

		try {
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(ss.toString());
			if (jsonParser.isHavingSuccess()) {
				String isSuccess = json.get("success").toString();
				System.out.println("json: " + json);
				if (isSuccess.equalsIgnoreCase("true")) {
					JOptionPane.showMessageDialog(new Seller(), "Transaction completed successful");
				}
			} else {
				String mesg = json.get("message").toString() + "";
				if (mesg != null && !mesg.isEmpty()) {
					JOptionPane.showMessageDialog(new Seller(), mesg);
				}
			}
		} catch (ParseException e) {
		}
		System.exit(0);
	}
}
