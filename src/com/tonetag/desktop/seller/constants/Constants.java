/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller.constants;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class contains Constant values and API lists
 */
public class Constants {

    public static final String SETTINGS_FILE_NAME = "settings.dat";
    public static final String SELLER_FILE_NAME = "seller.dat";
    public static final String CONFIG_FILE_NAME = "JublFood.Payment.API.config";
    
    // Shared Pref
    public static final String SHARED_PREF_NAME = "tonetag";
    public static final String SHARED_PREF_NAME_MERCHANT = "tonetag_merchant";
    public static final String SHARED_PREF_TAG_TYPE = "auth_type";
    public static final String SHARED_PREF_TAG_KEY = "auth_key";

    public static final int MODE_WALLET_CARD_PAYMENT = 1;
    public static final int MODE_IVR_PAYMENT = 2;
    
    public static final String SELLER_IDENTIFIER = "A0";


    // regex
    public static final String EMAIL_PATTERN ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // API's For Staging
    public static final String PRODUCTION_IP = "capp.tonetag.com";
    public static final String STAGING_IP = "cappstage.tonetag.com";
    public static final String DEVELOPMENT_IP = "cappdev.tonetag.com";
    public static final String TEMP_SERVER = "192.168.1.122:58627";
    
    public static final String TONETAG_DLL = "libToneTagWinx64.dll";
    public static final String TONETAG_TEMP_DLL = "ToneTagDll/temp/libToneTagWinx64.dll";
    
    public static final String DEVICE_MIC = "Primary Sound Capture Driver";
    private static final String IP = PRODUCTION_IP;
//    private static final String IP = STAGING_IP;
    
    public static final String TID_FILE_NAME = "FormatedTid.properties";
    
    public static final String TOKEN="Token 23116c8ea17c3dfdf65d1e191dcd495e33d82ed8ad759d5c71c65555ed812dd9";
    
    /*post :check_merchant_exists

         post :reset_password
         post :update_password
         get :merchant_info
         post :merchant_transaction_amount
         get :merchant_log_report
         post :transaction_id_present
         get :bank_encoder
         post :kyc_doc
         post :verify_merchant_otp
         post :send_merchant_verify_otp
         post :re_activate_otp
         post :kyc_upload_reminder
         post :update_merchant_details
         post :verify_teller_code
         post :day_and_monthly_transaction_count*/
    //String protocol = "http://";
    public static final String protocol = "https://";

    public static final String API_ONLINE_TRANSACTION = protocol+IP+"/api/v1/transactions/process_merchant";
    public static final String API_OFFLINE_TRANSACTION = protocol+IP+"/api/v1/transactions/do_offline_transaction";
    public static final String API_NUMBER_CHECK = protocol+IP+"/api/v2/merchants/check_merchant_exists"; //post
    public static final String API_MERCHANT_NUMBER_CHECK = protocol+IP+"/api/v2/merchants/pos_merchant_exists";
    public static final String API_TRANS__MAPPER_ID_URL = protocol+IP+"/api/v1/transactions/transaction_mapper_data";
    public static final String API_TRANSACTIONS_LOG = protocol+IP+"/api/v2/merchants/merchant_log_report";
    public static final String API_REFUND_AMOUNT = protocol+IP+"/api/v1/transactions/dominos_refund";
    public static final String API_STAUS = protocol+IP+"/api/v1/transactions/sym_client_txn_status";
    public static final String API_TRANSACTION_CANCEL = protocol+IP+"/api/v1/transactions/transaction_cancellation";
    
    
    public static final String API_MERCHANT_REG = protocol+IP+"/api/v2/merchants";
    public static final String API_MERCHANT_UPDATE_ACCOUNT = protocol+IP+"/api/v2/merchants/update_merchant_details";
    public static final String API_MERCHANT_ACCOUNT_VERIFY = protocol+IP+"/api/v2/merchants/transaction_id_present";
    public static final String API_MERCHANT_KYC_UPLOAD_URL = protocol+IP+"/api/v2/merchants/kyc_doc";
    public static final String API_MOBILE_VERIFY_CALLBACK = protocol +IP +"/api/v1/app_informations/give_miss_call";
    public static final String API_CARD_TRANSACTION = protocol + IP + "/api/v1/transactions/perform_card_transaction";
    public static final String API_CARD_AUTHENTICATION = protocol + IP + "/api/v1/transactions/authorize_card_transaction";
    public static final String API_VERIFY_MERCHANT_OTP = protocol + IP + "/api/v2/merchants/verify_merchant_otp";
    public static final String API_RESEND_MERCHANT_OTP = protocol + IP + "/api/v2/merchants/re_activate_otp";
    public static final String API_KYC_UPLOAD_REMINDER = protocol + IP + "/api/v2/merchants/kyc_upload_reminder";
    public static final String API_WALLET_TRANSACTION = protocol + IP + "/api/v1/transactions/perform_wallet_transaction";
    public static final String API_WALLET_AUTHENTICATION = protocol + IP + "/api/v1/transactions/authorize_wallet_transaction";
    public static final String API_APPLICATION_VERSION_CHECK = protocol+ IP +"/api/v1/app_informations/app_version";
    public static final String API_TELLER_OTP_VERIFY = protocol+IP+"/api/v2/merchant_tellers/verify_teller_otp";
    public static final String API_ADD_TELLER = protocol+IP+"/api/v2/merchant_tellers";
    public static final String API_REMOVE_TELLER = protocol+IP+"/api/v2/merchant_tellers/remove_teller";
    public static final String API_ADD_REFERRAL_CODE = protocol+IP+"/api/v2/merchants/verify_teller_code";
    public static final String API_ONLINE_TXN_ID = protocol+IP+"/api/v1/transactions/perform_online_card_transaction";
    public static final String API_ONLINE_TXN_ID_STATUS_CHECK = protocol+IP+"/api/v1/transactions/transaction_status";
}
