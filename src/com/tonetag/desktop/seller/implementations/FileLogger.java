/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller.implementations;

import com.tonetag.desktop.seller.interfaces.Logger;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author Ibrahim
 */
public class FileLogger implements Logger {
    
    private PrintWriter out;
    
    public FileLogger() throws IOException{
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
        String formattedDate = sdf.format(new Date());
        out = new PrintWriter(new FileWriter("ToneTag"+formattedDate+".log", true));
    }

    @Override
    public synchronized void writeEntry(Collection entry) {
        for(Iterator line = entry.iterator(); line.hasNext();){
            out.println(line.next());
        }
        out.println();
    }

    @Override
    public synchronized void writeEntry(String entry) {
        //System.out.println("entry : " + entry);
        out.println(entry);
        out.println();
    }

    @Override
    public void writeEntry(Exception e) {
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        out.println(e.getLocalizedMessage());
        for(int i = 0; i < stackTraceElements.length; i++){
            out.println(stackTraceElements[i].toString());
        }
        out.println();
    }
    
}
