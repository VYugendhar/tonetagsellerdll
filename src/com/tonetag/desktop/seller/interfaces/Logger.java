package com.tonetag.desktop.seller.interfaces;

import java.util.Collection;

/**
 * common interface for the logger
 * 
 * @author Ibrahim Sankadal
 */
public interface Logger {
    
    /**
     * Function used to write log of collection
     * 
     * @param entry collection of logs.
     */
    public void writeEntry(Collection entry); // Write list of lines
    
    /**
     * Function used to write a single line log.
     * 
     * @param entry log string.
     */
    public void writeEntry(String entry); // Write single line
    
    /**
     * Function used to write exceptions found 
     * 
     * @param e exception object
     */
    public void writeEntry(Exception e);
    
}
