
package com.tonetag.desktop.seller.panels;

import com.tonetag.desktop.seller.Seller;
import com.tonetag.desktop.seller.bean.Bill;
//import com.tonetag.desktop.seller.bean.Jubl;
import com.tonetag.desktop.seller.bean.Merchant;
//import com.tonetag.desktop.seller.bean.Orders;
import com.tonetag.desktop.seller.bean.Settings;
import com.tonetag.desktop.seller.bean.Teller;
import com.tonetag.desktop.seller.constants.Constants;
import com.tonetag.desktop.seller.tasks.JsonParser;
import com.tonetag.desktop.seller.tasks.NetworkAsyncTask;
import com.tonetag.desktop.seller.tasks.Utils;
import com.tonetag.desktop.tone.SoundPlayer;
import com.tonetag.desktop.tone.SoundRecorder;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author dev1
 */
public class DominosTransactionPanel extends javax.swing.JPanel {
    private static final String TRIGGER_FREQUENCY = "450";
    private static final String TRIGGER_SUCCESS_FREQUENCY = "600";
    private static final String TRIGGER_FAILURE_FREQUENCY = "900";
    
    private static String FILE_PATH = "C:\\Program Files (x86)\\Hustle\\CCD-TRV\\";
    static {
	/*String arch = System.getProperty("sun.arch.data.model");
        //System.out.println("Arch " + arch);
        if (arch.equals("32")) {
            System.loadLibrary("ToneTagLX86");
        }else{
            System.loadLibrary("ToneTagLX64");
        }*/
        
        if(Utils.isUnix()){
            if(Utils.isX86()){
                System.loadLibrary("ToneTagLX86");
            }
            if(Utils.isX64()){
                System.loadLibrary("ToneTagLX64");
            }
        }
        
        if(Utils.isWindows()){
            //System.loadLibrary("ToneTagWin");
            if(new File("C:\\Program Files (x86)\\").isDirectory()){
                FILE_PATH = "C:\\Program Files (x86)\\Hustle\\CCD-TRV\\";
            }else{
                FILE_PATH = "C:\\Program Files\\Hustle\\CCD-TRV\\";
            }
            //System.load("C:\\tonetag\\lib\\libToneTagWin.dll");
        	System.load(new File(Constants.TONETAG_DLL).getAbsolutePath());
//            System.load("C:\\tonetag\\lib\\libToneTagWinx64.dll");
            //System.load("D:\\Storage\\NewDevelopment\\ToneTagNative\\dist\\libToneTagWin.dll");
        }
        if(Utils.isMac()){
            System.loadLibrary("ToneTagMac");
        }
    }

    private Seller mainForm;
    
    private String amount = "0.00";
    
    private Bill bill;
    
    private Merchant merchant;
    private List<Teller> tellers; 
    private String selectedTellerCode;
    private javax.swing.JPanel jpBasePanel;
    
    private static final int TXN_INITIALISATION = 0;
    
    private static final int TXN_AUTHENTICATION = 1;
    
    private int mode;
    
    private boolean encryptedToneReceived = false;
    private boolean trackIdReceived = false;

    private int whichApiCall = 1;

    private static final int TXN_API_CALL = 2;
    private static final int OTP_API_CALL = 1;
    
    private static final int TXN_ONE_WAY = 1;
    private static final int TXN_TWO_WAY = 0;

    private int txnFlow = 0;
    private char customerIdentificationFlag = '0';

    private static final char BIC_CARD_IDENTIFIER = 'C';
    private static final char BIC_WALLET_IDENTIFIER = 'W';
    private static final char BIC_OTHERS_IDENTIFIER = 'O';
    private static final char BIC_BANK_IDENTIFIER = 'B';

    private static final char NULL_MODE_OF_PAYMENT = 'N';

    public static final int TRANSACTION_RESULT_SUCCESS = 1;
    public static final int TRANSACTION_RESULT_FAILED = 0;
    
    private static final String RESULT_SUCCESS_STRING = "SUCCESS";
    private static final String RESULT_FAILURE_STRING = "FAIL";
    
    private char modeOfPayment = 'N';
    private String track_ID = null;
    private String bic;
    private String cAppId;
    private String tokenIDOrPhoneNum = "";
    //private String tellerCode;
    private String receivedString;
    private List<NameValuePair> params = null;
    private NetworkAsyncTask mNetworkAsyncTask;
    
    
    /**
     * Creates new form TransactionPanel
     */
    public DominosTransactionPanel(Seller mainForm) {
        this.mainForm = mainForm;
        this.tellers = mainForm.tellers;
        this.merchant = mainForm.merchant;
        this.selectedTellerCode = mainForm.tellers.get(0).getTeller_code();//mainForm.selectedTellerCode;
        this.jpBasePanel = mainForm.jpBasePanel;
        this.micDriver = Constants.DEVICE_MIC;//mainForm.mSettings.getMicMixer();
        this.speakerDriver = mainForm.mSettings.getSpeakerMixer();
        
        /*mSoundPlayer = new SoundPlayer(speakerDriver);
        mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
        mSoundRecorder = new SoundRecorder(micDriver);
        mSoundRecorder.setOnDataFoundListener(new OnSoundDataReceived());*/
    }
    
    public void setAmount(String amount){
        this.amount = amount;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlblProcessTxn = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jlblAmount = new javax.swing.JLabel();
        jpTransaction = new javax.swing.JPanel();
        jbProceed = new javax.swing.JButton();
        jpTransactionProgress = new javax.swing.JPanel();
        jlblProgressText = new javax.swing.JLabel();
        pbTransactionProgress = new javax.swing.JProgressBar();
        jbCancel = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(600, 600));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jlblProcessTxn.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jlblProcessTxn.setText("<html>Processing Transaction For</html>");
        add(jlblProcessTxn, new org.netbeans.lib.awtextra.AbsoluteConstraints(139, 12, -1, 59));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jLabel1.setText("Rs");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(223, 89, 60, 56));

        jlblAmount.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jlblAmount.setText(this.amount);
        add(jlblAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(289, 89, 227, 56));

        jpTransaction.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jbProceed.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jbProceed.setText("Proceed");
        jbProceed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbProceedActionPerformed(evt);
            }
        });
        jpTransaction.add(jbProceed, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, 552, 76));

        add(jpTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 382, 576, -1));

        jlblProgressText.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jlblProgressText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblProgressText.setText("Progress");
        jlblProgressText.setFocusCycleRoot(true);

        javax.swing.GroupLayout jpTransactionProgressLayout = new javax.swing.GroupLayout(jpTransactionProgress);
        jpTransactionProgress.setLayout(jpTransactionProgressLayout);
        jpTransactionProgressLayout.setHorizontalGroup(
            jpTransactionProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpTransactionProgressLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpTransactionProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pbTransactionProgress, javax.swing.GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)
                    .addComponent(jlblProgressText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpTransactionProgressLayout.setVerticalGroup(
            jpTransactionProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpTransactionProgressLayout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(jlblProgressText)
                .addGap(18, 18, 18)
                .addComponent(pbTransactionProgress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jpTransactionProgress, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 382, 576, 90));

        jbCancel.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jbCancel.setText("Cancel");
        jbCancel.setToolTipText("Click to Cancel transaction");
        jbCancel.setFocusCycleRoot(true);
        jbCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelActionPerformed(evt);
            }
        });
        add(jbCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 512, 576, 76));
    }// </editor-fold>//GEN-END:initComponents

    private void jbProceedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbProceedActionPerformed
        // TODO add your handling code here:
        
        if(speakerDriver != null ){
            amount = jlblAmount.getText().trim();
        
            // initialise flags
            encryptedToneReceived = false;
            trackIdReceived = false;

            // Start transaction process here
            // triggerToResultPanel();
            whichApiCall = OTP_API_CALL;
            mode = TXN_INITIALISATION;
            jlblProgressText.setText("Sending Invoice to customer");
            stopRecorder();
            stopPlayer();
            startRecorder(SoundRecorder._30_BYTES_RECORD_MODE, SoundRecorder.SENDER_POS);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                        startPlayer(SoundRecorder._30_BYTES_RECORD_MODE, TRIGGER_FREQUENCY);
                    } catch (InterruptedException ex) {
                       // Logger.getLogger(DominosTransactionPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }).start();
            
            //startRecorder(SoundRecorder.STRING_RECORD_MODE, SoundRecorder.SENDER_CUSTOMER);
            hideCancelAndSeek();
            //hideAndSeek();
        }else{
            jlblProgressText.setText("Invalid Audio Device selected");
            System.out.println("Device not selected");
        }

    }//GEN-LAST:event_jbProceedActionPerformed

    private void playTone(String data){
        if(speakerDriver != null){
            mSoundPlayer = new SoundPlayer(speakerDriver);
            mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    createDialog("Sending String Over Sound...");
                }
            }).start();*/

            mSoundPlayer.sendString(data);
        }
    }
    
    private void startRecordingTone(){
        if(micDriver != null){
            mSoundRecorder = new SoundRecorder(micDriver);
            mSoundRecorder.setOnDataFoundListener(new OnSoundDataReceived());
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    createDialog("Receiver is Running...");
                }
            }).start();*/
            mSoundRecorder.startRecording(SoundRecorder.STRING_RECORD_MODE, 
                    SoundRecorder.SENDER_CUSTOMER, false, 3000, 16);
        }
    }
    
    private void hideAndSeek(){
        if(jpTransaction.isVisible()){
            jpTransaction.setVisible(false);
            pbTransactionProgress.setIndeterminate(true);
            jlblProgressText.setText("connecting server, please wait...");
            jpTransactionProgress.setVisible(true);
        }else{
        	if(!jbCancel.isVisible())
        		jbCancel.setVisible(true);
            jpTransaction.setVisible(true);
            pbTransactionProgress.setIndeterminate(false);
            jpTransactionProgress.setVisible(false);
        }
    }
    
    private void hideCancelAndSeek(){
        if(jpTransaction.isVisible()){
        	jbCancel.setVisible(false);
            jpTransaction.setVisible(false);
            pbTransactionProgress.setIndeterminate(true);
            jlblProgressText.setText("connecting server, please wait...");
            jpTransactionProgress.setVisible(true);
            
        }else{
        	jbCancel.setVisible(true);
            jpTransaction.setVisible(true);
            pbTransactionProgress.setIndeterminate(false);
            jpTransactionProgress.setVisible(false);
        }
    }
    
    private void apiRequestToSendOTP(final int apiCall, final String s){
        new Thread(new Runnable() {
            @Override
            public void run() {
                /*try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                if (apiCall == OTP_API_CALL) {
                    //System.out.println("OTP Call For " + s);
                    //CommentTAG, "OTP Call For " + s);

                    //bic = s.substring(s.length()-4,s.length());
                    //char bicIdentifier = bic.charAt(0);
                    //modeOfPayment = bicIdentifier;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // TODO change progress status here
                            //ivProgressStep.setImageResource(R.drawable.p_layer_two);
                            switch (modeOfPayment){
                                case 'C':
                                    //Api call for card transaction
                                    callBackend(s, apiCall, Constants.API_CARD_TRANSACTION);
                                    break;
                                case 'W':
                                    txnFlow = Integer.parseInt(s.charAt(10)+"");
                                    callBackend(s, apiCall, Constants.API_WALLET_TRANSACTION);
                                    // API call for wallet transaction
                                    break;
                                case 'B':
                                    // API call for Bank Transaction
                                    break;
                                case 'O':
                                    //API call for Other Transaction
                                    break;
                                default:
                                    //L.e(TAG, " somthing wrong");
                                    break;
                                }
                            }
                        }).start();

                    }
                if (apiCall == TXN_API_CALL){
                    encryptedToneReceived = true;
                    //System.out.println("TXN Call For " + s);
                    //trackIdReceived = false;
                    //Log.e(TAG, "TXN Call For " + s);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (encryptedToneReceived == true && trackIdReceived == true) {
                                // TODO change progress status here
                                //ivProgressStep.setImageResource(R.drawable.p_layer_three);
                                //showLoadingDialog(getResources().getString(R.string.process_msg_five));
                                switch (modeOfPayment) {
                                    case 'C':
                                        //Api call for card transaction
                                        callBackend(s, apiCall, Constants.API_CARD_AUTHENTICATION);
                                        break;
                                    case 'W':
                                        // API call for wallet transaction
                                        callBackend(s, apiCall, Constants.API_WALLET_AUTHENTICATION);
                                        break;
                                    case 'B':
                                        // API call for Bank Transaction
                                        break;
                                    case 'O':
                                        //API call for Other Transaction
                                        break;
                                    default:
                                        //L.e(TAG, " something wrong");
                                        break;
                                }
                            }
                        }
                    }).start();


                    // if customer used wallet
                    /*if (mDialog != null) {
                        mDialog.cancel();
                    }*/
                    /*Log.d(TAG, "!data received: " + s);
                    Intent intent = new Intent(mContext, CardDetailActivity.class);
                    intent.putExtra("mode", "wallet");
                    intent.putExtra("amount", Double.parseDouble(amount));
                    startActivity(intent);*/

                }
            }
        }).start();
    }
    
    private void callBackend(final String data, final int apiCall, final String API){

        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {*/
                txnProcess(data, apiCall, API);
            /*}
        });*/

    }
    
    
    private void dominosTxnProcess(String data, String API){
        params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("mid", merchant.getApp_id()));
        params.add(new BasicNameValuePair("amount", amount));
        params.add(new BasicNameValuePair("bin", "W111"));
        params.add(new BasicNameValuePair("teller_code", selectedTellerCode));
        params.add(new BasicNameValuePair("refcode", data));
        params.add(new BasicNameValuePair("tid", data.trim().substring(0, 10)));
        this.mainForm.track_Ref_ID = data;
//        params.add(new BasicNameValuePair("merchant_misc", ""));
        
        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), API, NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);

        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                //CommentTAG, (String) obj);
                JsonParser jsonParser = new JsonParser((String) obj);
                
                if(jsonParser.isHavingSuccess()){
                    track_ID = jsonParser.getTrackId();
                    isTxnFailed = false;
                    stopRecorder();
                	call_TransactionMapp(track_ID);

                	 new Thread(new Runnable() {
                         @Override
                         public void run() {
                             try {
                                 Thread.sleep(500);
                             	startPlayer(SoundRecorder._30_BYTES_RECORD_MODE, TRIGGER_SUCCESS_FREQUENCY);
                             } catch (InterruptedException ex) {
                                // Logger.getLogger(DominosTransactionPanel.class.getName()).log(Level.SEVERE, null, ex);
                             }
                         }
                     }).start();
                    genBill(track_ID, TRANSACTION_RESULT_SUCCESS,jsonParser.getMessage());
                    mainForm.track_Ref_ID = null;
                }else{
                    isTxnFailed = true;
                    stopRecorder();
                    //System.out.println("Txn init faild ");
                    //if(track_ID == null || track_ID.isEmpty()) track_ID = "";
               	 new Thread(new Runnable() {
                     @Override
                     public void run() {
                         try {
                             Thread.sleep(500);
                             startPlayer(SoundRecorder._30_BYTES_RECORD_MODE, TRIGGER_FAILURE_FREQUENCY);
                         } catch (InterruptedException ex) {
                            // Logger.getLogger(DominosTransactionPanel.class.getName()).log(Level.SEVERE, null, ex);
                         }
                     }
                 }).start();
                 if(track_ID != null && !track_ID.isEmpty())
                 	callCancelTransProcess(track_ID);
                    genBill("", TRANSACTION_RESULT_FAILED, jsonParser.getMessage());
                }
                
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
                String message = "";
                System.out.println("responsePhrase: "+responsePhrase);
                if (responsePhrase != null) message = responsePhrase;
                if(track_ID == null || track_ID.isEmpty()) track_ID = "";
                
              	 new Thread(new Runnable() {
                     @Override
                     public void run() {
                         try {
                             Thread.sleep(500);
                             startPlayer(SoundRecorder._30_BYTES_RECORD_MODE, TRIGGER_FAILURE_FREQUENCY);
                         } catch (InterruptedException ex) {
                            // Logger.getLogger(DominosTransactionPanel.class.getName()).log(Level.SEVERE, null, ex);
                         }
                     }
                 }).start();
                //stopPlayer();
                stopRecorder();
                if(track_ID != null && !track_ID.isEmpty())
                	callCancelTransProcess(track_ID);
//                genBill(track_ID, TRANSACTION_RESULT_FAILED, message);
                hideAndSeek();
                //L.e(TAG, responsePhrase);
                // TODO show Alert dialog here
                //showAlertDialog(ProcessActivity.this, null, message, null, HomeActivity.class);

            }
        });

        mNetworkAsyncTask.start();
        
    }
    
    public boolean call_TransactionMapp(String transId){
    	try {
    	String url = Constants.API_TRANS__MAPPER_ID_URL;
    	String responseMessage = null;
        int responseCode = 500;
        String data = "";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        
        String locCode = this.mainForm.storeNumber;//params.get("location_code").toString();
        String compNm = this.mainForm.terminalNumber;// params.get("comp_name").toString();
        String ordId = this.mainForm.orderID;//params.get("order_id").toString();
        String phone = this.mainForm.phone;//params.get("datetime").toString();
        String trnsId = transId;
        
    	data = "data={"+"\"transaction_id\""+":\""+trnsId+"\","+"\"location_code\""+":\""+locCode+"\","
    			+"\"comp_name\""+":\""+compNm+"\","+"\"order_id\""+":\""+ordId+"\","+"\"phone\""+":\""+phone+"\"}";
    	
        con.setRequestMethod("POST");
        //con.setRequestProperty("Authorization", "Token 23116c8ea17c3dfdf65d1e191dcd495e33d82ed8ad759d5c71c65555ed812dd9");
        con.setRequestProperty("Authorization", Constants.TOKEN);
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();

        responseCode = con.getResponseCode();

        StringBuilder response = new StringBuilder();
        if(responseCode == 200){
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        }else{
            response.append(con.getResponseMessage());
        }
        responseMessage = response.toString();
        System.out.println("res: "+responseMessage);
        if(responseMessage.contains("true")){
        	return true;
        } else {
        	call_TransactionMapp(transId);
        }
		} catch (Exception e) {
		}
        return false;
	}
 
    private void callCancelTransProcess(String transId){
    	
        List<NameValuePair> params = new ArrayList<NameValuePair>();
       params.add(new BasicNameValuePair("transaction_id", transId));
        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), Constants.API_TRANSACTION_CANCEL,NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);
        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                JsonParser jsonParser = new JsonParser((String) obj);
                if (jsonParser.isHavingSuccess()){
       				JOptionPane.showMessageDialog(new Seller(), "Transaction hasbeen canceled");
//           			hideAndSeek();
                }
            }
            @Override
            public void onProcessProgress(int value) {
            }
            @Override
            public void onErrorResponse(String responsePhrase) {
            }
        });
        mNetworkAsyncTask.start();
    }
    
    private void txnProcess(String data, final int apiCall, String API){
        //Comment"data", data);
        params = new ArrayList<NameValuePair>();
        //System.out.println("txnProcess Called");
        if (apiCall == OTP_API_CALL){

            if (modeOfPayment == BIC_CARD_IDENTIFIER){
                tokenIDOrPhoneNum = data.substring(0,data.length()-6);
                params.add(new BasicNameValuePair("token_id", tokenIDOrPhoneNum));
                params.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));

            }else if (modeOfPayment == BIC_WALLET_IDENTIFIER){
                tokenIDOrPhoneNum = data.substring(0,data.length()-6);
                //cAppId = data.substring(data.length()-12,data.length()-4);

                params.add(new BasicNameValuePair("wallet_id", tokenIDOrPhoneNum));
                params.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));
                //params.add(new BasicNameValuePair("c_app_id", cAppId));
            }
            params.add(new BasicNameValuePair("bic", bic));
            params.add(new BasicNameValuePair("amount", amount));

            //L.e(TAG, "tokenIDOrPhoneNum : " + tokenIDOrPhoneNum  + ", m_app_id: "+ merchant.getApp_id() + ", bic: " + bic + ", amount: " + amount);

        }
        if (apiCall == TXN_API_CALL){
            if (txnFlow == TXN_TWO_WAY) {
                cAppId = data.substring(data.length() - 8, data.length());
                //L.e(TAG, "track_ID" + track_ID);
                params.add(new BasicNameValuePair("data", data));
            }
            //cAppId = data.substring(data.length()-8,data.length());
            //L.e(TAG, "track_ID" + track_ID);
            //params.add(new BasicNameValuePair("data", data));
            params.add(new BasicNameValuePair("track_id", track_ID));
            params.add(new BasicNameValuePair("token_id", tokenIDOrPhoneNum));
            params.add(new BasicNameValuePair("teller_code", selectedTellerCode));

            if (modeOfPayment == BIC_WALLET_IDENTIFIER){
                params.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));
                params.add(new BasicNameValuePair("c_app_id", cAppId));
            }

            /*L.e(TAG, "data : " + data + ", c_app_id: " + cAppId + ", m_app_id: " +
                    merchant.getApp_id() + ", tokenIDOrPhoneNum : " + tokenIDOrPhoneNum  +
                    ", track_id: " + track_ID + ", teller_code :" + selectedTellerCode);*/
        }

        /*if (mDialog != null){
            mDialog.setMessage(getResources().getString(R.string.get_txn_info));
        }*/

        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), API, NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);

        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                //CommentTAG, (String) obj);
                JsonParser jsonParser = new JsonParser((String) obj);

                if (apiCall == OTP_API_CALL){

                    /*Merchant merchant = db.getUser();
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    String kycStatus = jsonParser.getTagKycStatus();
                    boolean isKycUploaded = jsonParser.isKycUploaded();*/

                    if (jsonParser.isHavingSuccess()){
                        track_ID = jsonParser.getTrackId();

                        /*merchant.setKyc_status(kycStatus);
                        merchant.setKyc_uploaded(isKycUploaded);
                        db.updateMerchantDetail(merchant);
                        db.close();

                        editor.putString(PREF_KEY_KYC, merchant.getKyc_status());
                        editor.putBoolean(PREF_KEY_KYC_UPLOADED, merchant.getKyc_uploaded());

                        editor.commit();*/

                        /*if (whichApiCall == OTP_API_CALL){
                            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                            profileSet = true;
                            mSoundPlayer.setDeviceVolume(volLevel);
                            mSoundPlayer.sendAmount(Float.parseFloat(amount));
                        }*/
                        //encryptedToneReceived = false;
                        if(txnFlow == TXN_ONE_WAY){
                            //System.out.println("One way found at api call");
                            whichApiCall = TXN_API_CALL;
                            encryptedToneReceived = true;
                        }
                        trackIdReceived = true;
                        isTxnFailed = false;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                jlblProgressText.setText("Customer Identified...");
                            }
                        }).start();
                        
                        if (encryptedToneReceived == true){
                            //System.out.println("encryptedToneReceived = true");
                            apiRequestToSendOTP(whichApiCall, receivedString);
                        }
                        
                    }else{
                        /*merchant.setKyc_status(kycStatus);
                        merchant.setKyc_uploaded(isKycUploaded);
                        db.updateMerchantDetail(merchant);
                        db.close();

                        editor.putString(PREF_KEY_KYC, merchant.getKyc_status());
                        editor.putBoolean(PREF_KEY_KYC_UPLOADED, merchant.getKyc_uploaded());

                        editor.commit();*/

                        //L.e(TAG, jsonParser.getMessage());
                        isTxnFailed = true;
                        stopRecorder();
                        //System.out.println("Txn init faild ");
                        genBill("", TRANSACTION_RESULT_FAILED, jsonParser.getMessage());
                    }
                }
                if (apiCall == TXN_API_CALL){
                    if (jsonParser.isHavingSuccess()){
                        //Log.e("hi", "i called");
                        // TODO change status image here
                        //ivProgressStep.setImageResource(R.drawable.p_layer_four);
                        modeOfPayment = NULL_MODE_OF_PAYMENT;
                        isTxnFailed = false;
                        genBill(track_ID, TRANSACTION_RESULT_SUCCESS,jsonParser.getMessage());
                    }else{
                        isTxnFailed = true;
                        genBill(track_ID, TRANSACTION_RESULT_FAILED, jsonParser.getMessage());
                        //finish();
                        //L.e(TAG, jsonParser.getMessage());
                    }
                }
                /*if (!jsonParser.isHavingError()) {
                    Map<String, String> map = jsonParser.getTxnValues();
                    String bic = map.get("bic");
                    String walletSpec = map.get("walletspec");
                    //L.e(TAG, "bic : " + bic  + " walletspec : "+ walletSpec);
                    genBill("123456");
                }else{
                    //L.e(TAG, "error ");
                }*/
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
                String message = "";
                if (responsePhrase != null) message = responsePhrase;
                if(track_ID == null || track_ID.isEmpty()) track_ID = "";
                stopPlayer();
                stopRecorder();
                genBill(track_ID, TRANSACTION_RESULT_FAILED, message);
                //L.e(TAG, responsePhrase);
                // TODO show Alert dialog here
                //showAlertDialog(ProcessActivity.this, null, message, null, HomeActivity.class);

            }
        });

        mNetworkAsyncTask.start();
    }
    
    private void genBill(String txn_id, int result, String message){
        
        // invoke bill here
        bill = new Bill();
        bill.status = result;
        bill.message = message;
        bill.txnNumber = txn_id;
        bill.amount = amount;
        if(result == TRANSACTION_RESULT_SUCCESS){
            writeToXML(new String[]{amount, RESULT_SUCCESS_STRING, txn_id});
        }else{
            writeToXML(new String[]{amount, RESULT_FAILURE_STRING, txn_id});
        }
        triggerToResultPanel();
        /*Intent intent = new Intent(mContext, ReceiptActivity.class);
        intent.putExtra("mode", "wallet");
        intent.putExtra("amount", Double.parseDouble(amount));
        intent.putExtra("result", result);
        intent.putExtra("txn_id", txn_id);
        intent.putExtra("msg", message);
        startActivity(intent);
        finish();*/
    }
    
    private void writeToXML(String[] values){/*
        JAXBContext jAXBContext;
        Jubl jubl;
        Orders order;
        File file;
        //java.net.InetAddress i;
        Properties properties = System.getProperties();
        try {
            //i = java.net.InetAddress.getLocalHost();
            //String clientName = System.getenv("CLIENTNAME");
            String clientName = "ToneTag-PC";
            file = new File(FILE_PATH + clientName.trim()+".xml");
            order = new Orders(values[0], values[1], values[2]);
            List list = new ArrayList<Orders>();
            list.add(order);
            jubl = new Jubl(list);
            jAXBContext = JAXBContext.newInstance(com.tonetag.desktop.seller.bean.Jubl.class);

            Marshaller jaxbMarshaller = jAXBContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(jubl, file);
            jaxbMarshaller.marshal(jubl, System.out);

        } catch (JAXBException ex) {
            ex.printStackTrace();
            System.out.println(ex.getLocalizedMessage());
            //Logger.getLogger(UpdateXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
            System.out.println(ex.getLocalizedMessage());
        }
    */}
    
    private void jbCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCancelActionPerformed
        // TODO add your handling code here:
        
        // Immidiate cancel of transaction
        stopRecorder();
        stopPlayer();
        disposeFrame();
        //triggerToMainPanel();
        
    }//GEN-LAST:event_jbCancelActionPerformed

    private String speakerDriver = null;
    private String micDriver = null;
    
    private void startPlayer(int type, String data){
        /*if(mSoundPlayer != null){
            mSoundPlayer = new SoundPlayer();
        }else{
            mSoundPlayer.setOnPlaybackFinishedListener(null);
        }*/
        
        if(speakerDriver != null){
            mSoundPlayer = new SoundPlayer(speakerDriver);
            System.out.println("Speaker Driver : " + speakerDriver);
            mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    createDialog("Sending String Over Sound...");
                }
            }).start();*/

            switch(type){
                case SoundRecorder.AMOUNT_RECORD_MODE:
                    mSoundPlayer.sendAmount(Float.parseFloat(data));
                    break;
                case SoundRecorder.STRING_RECORD_MODE:
                    mSoundPlayer.sendString(data);
                    break;
                case SoundRecorder._30_BYTES_RECORD_MODE:
                    mSoundPlayer.genTerminalTriggerTone(data);
                    break;
            }
           
        }
        
    }
    
    private void startRecorder(int type, int from){
        /*if(mSoundRecorder != null){
            mSoundRecorder = new SoundRecorder();
        }else{
            mSoundRecorder.setOnDataFoundListener(null);
        }*/
        
        //System.out.println("Recorder started");
        if(micDriver != null){
            mSoundRecorder = new SoundRecorder(micDriver);
            mSoundRecorder.setOnDataFoundListener(new OnSoundDataReceived());
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    createDialog("Receiver is Running...");
                }
            }).start();*/
            /*mSoundRecorder.startRecording(SoundRecorder.STRING_RECORD_MODE, 
                    SoundRecorder.SENDER_CUSTOMER, false, 3000, 16);*/
            try{
                mSoundRecorder.startRecording(type, from, false, 3000, 0);
            }catch(Exception ex){
                JOptionPane.showMessageDialog(mainForm, ex.getLocalizedMessage(),
                        "Audio Input Error", JOptionPane.ERROR_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(mainForm, "Mic Not Selected",
                        "Audio Input Error", JOptionPane.ERROR_MESSAGE);
        }
        
        //mSoundRecorder.startRecording(type, from, false, 3000, 0);
       
        
    }
    
    /*private void stopRecorder(){
        if(mSoundRecorder != null){
            if(mSoundRecorder.isRecordingOn()){
                mSoundRecorder.stopRecording();
            }
        }
    }*/
    
    private class OnSoundDataPlaybackFinished implements SoundPlayer.OnPlaybackFinishedListener{

        @Override
        public void onPlaybackFinished() {
            /*mode = TXN_AUTHENTICATION;
            whichApiCall = TXN_API_CALL;
            jlblProgressText.setText("Generating OTP for Customer");
            if(txnFlow == TXN_TWO_WAY) {
                startRecorder(SoundRecorder.TXN_STRING_RECORD_MODE, SoundRecorder.SENDER_CUSTOMER);
            }else{
                apiRequestToSendOTP(whichApiCall, ""+bic);
            }*/
            
        }
        
        
    }
    
    private class OnSoundDataReceived implements SoundRecorder.OnDataFoundListener{

        @Override
        public void onDataFound(String data, int i, boolean bln, short s) {
            //System.out.println("Received data : " + data);
            /*if (mode == TXN_INITIALISATION) {
                startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, jlblAmount.getText());
            }*/
            try{
                // call the api to send customer info and send the amount to customer
                receivedString = data;
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showLoadingDialog(getResources().getString(R.string.process_msg_three));
                    }
                });*/
                /*if (whichApiCall == OTP_API_CALL){
                    //audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                    
                    customerIdentificationFlag = data.charAt(11);
                    bic = data.substring(data.length()-4,data.length());
                    char bicIdentifier = bic.charAt(0);
                    modeOfPayment = bicIdentifier;
                    //System.out.println("Mode of payment : " + modeOfPayment);

                    //System.out.println("customerIdentificationFlag is " + customerIdentificationFlag);

                    String totalAmount = customerIdentificationFlag + amount;
                    //System.out.println("totalAmount is " + totalAmount);
                    if(modeOfPayment == 'W'){
                        int flow = Integer.parseInt(data.charAt(10)+"");
                        if(flow == TXN_ONE_WAY){
                            //System.out.println("Mode One way");
                            //apiRequestToSendOTP(whichApiCall, ""+bic);
                            jlblProgressText.setText("Initiating Transaction please wait..");
                        }else{
                            //System.out.println("Mode two way");
                            jlblProgressText.setText("Sending Invoce to Customer");
                            startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, totalAmount);
                        }

                    }else{
                        jlblProgressText.setText("Sending Invoce to Customer");
                        startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, totalAmount);
                    }
                    //jlblProgressText.setText("Sending Invoce to Customer");
                    //startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, totalAmount);
                }
                if(whichApiCall == TXN_API_CALL){
                    jlblProgressText.setText("Authenticating Transaction please wait..");
                }*/

                System.out.println("Received Data: "+data);
                jlblProgressText.setText("Transaction Initiated please wait for customer response..");
                //apiRequestToSendOTP(whichApiCall, data);
                dominosTxnProcess(data, Constants.API_ONLINE_TRANSACTION);
            }catch(Exception e){
                stopRecorder();
                stopPlayer();
                triggerToMainPanel();
            }
            
        }

        @Override
        public void onDataFound(long l, int i, short s) {
        }
        
    }

    

    private void triggerToResultPanel(){
        ResultPanel rp = new ResultPanel(mainForm);
        
        rp.setResultValue(bill);
        
        jpBasePanel.removeAll();
        jpBasePanel.add(rp);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    private void triggerToMainPanel(){
        jpBasePanel.removeAll();
        //jpBasePanel.add(new AmountPanel(mainForm, amount));
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    private void disposeFrame(){
        //stopRecorder();
        mainForm.dispose();
    }
    
    private void stopRecorder(){
        //System.out.println("Recorder Stoped");
        if(mSoundRecorder != null){
            //if(mSoundRecorder.isRecordingOn())
                mSoundRecorder.stopRecording();
                //mSoundRecorder = null;
        }  
    }
    
    private Settings createSetting() {
        return mainForm.mSettings;
    }
    
    private void stopPlayer(){
        if(mSoundPlayer != null){
            //mSoundPlayer = null;
        }
    }
    private SoundRecorder mSoundRecorder;
    private SoundPlayer mSoundPlayer;
    private boolean isTxnFailed = false;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton jbCancel;
    private javax.swing.JButton jbProceed;
    private javax.swing.JLabel jlblAmount;
    private javax.swing.JLabel jlblProcessTxn;
    private javax.swing.JLabel jlblProgressText;
    private javax.swing.JPanel jpTransaction;
    private javax.swing.JPanel jpTransactionProgress;
    private javax.swing.JProgressBar pbTransactionProgress;
    // End of variables declaration//GEN-END:variables
}
