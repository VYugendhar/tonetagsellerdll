/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller.panels;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.tonetag.desktop.seller.Seller;
import com.tonetag.desktop.seller.bean.Merchant;
import com.tonetag.desktop.seller.bean.Settings;
import com.tonetag.desktop.seller.bean.Teller;
import com.tonetag.desktop.seller.constants.Constants;
import com.tonetag.desktop.seller.tasks.JsonParser;
import com.tonetag.desktop.seller.tasks.NetworkAsyncTask;
import com.tonetag.desktop.seller.tasks.Utils;

/**
 *
 * @author dev1
 */
public class RegisterPanel extends javax.swing.JPanel {
    
    private Seller mainForm;
    private javax.swing.JPanel jpBasePanel;
    private List<NameValuePair> params = null;
    private NetworkAsyncTask mNetworkAsyncTask;
    public Merchant merchant;
    public List<Teller> tellers;
    
    private String callStatus = "init";

    /**
     * Creates new form RegisterPanel
     */
    public RegisterPanel(Seller mainForm) {
        this.mainForm = mainForm;
        this.jpBasePanel = mainForm.jpBasePanel;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jbVerify = new javax.swing.JButton();
        jpKeyPad = new javax.swing.JPanel();
        jbtn1 = new javax.swing.JButton();
        jbtn2 = new javax.swing.JButton();
        jbtn3 = new javax.swing.JButton();
        jbtn4 = new javax.swing.JButton();
        jbtn5 = new javax.swing.JButton();
        jbtn6 = new javax.swing.JButton();
        jbtn7 = new javax.swing.JButton();
        jbtn8 = new javax.swing.JButton();
        jbtn9 = new javax.swing.JButton();
        jbtn0 = new javax.swing.JButton();
        jbtnDelete = new javax.swing.JButton();
        jbtnVirtualKeyboard = new javax.swing.JButton();
        jpProgressStatus = new javax.swing.JPanel();
        jlblStatus = new javax.swing.JLabel();
        jpBusyBar = new javax.swing.JProgressBar();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jlblError = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(600, 600));

        jbVerify.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jbVerify.setText("Verify");
        jbVerify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbVerifyActionPerformed(evt);
            }
        });

        jbtn1.setText("1");
        jbtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn2.setText("2");
        jbtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn3.setText("3");
        jbtn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn4.setText("4");
        jbtn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn5.setText("5");
        jbtn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn6.setText("6");
        jbtn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn7.setText("7");
        jbtn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn8.setText("8");
        jbtn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn9.setText("9");
        jbtn9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });

        jbtn0.setText("0");
        jbtn0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn0ActionPerformed(evt);
            }
        });
        
        jbtnDelete.setText("Del");
        jbtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                jbtn0ActionPerformed(evt);
                virtualKeyBordActionPerformed(evt);
            }
        });
/*
        javax.swing.GroupLayout jpKeyPadLayout = new javax.swing.GroupLayout(jpKeyPad);
        jpKeyPad.setLayout(jpKeyPadLayout);
        jpKeyPadLayout.setHorizontalGroup(
            jpKeyPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpKeyPadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jbtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn0)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnDelete)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jpKeyPadLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jbtn0, jbtn1, jbtn2, jbtn3, jbtn4, jbtn5, jbtn6, jbtn7, jbtn8, jbtn9, jbtnDelete});

        jpKeyPadLayout.setVerticalGroup(
            jpKeyPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpKeyPadLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jpKeyPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn5)
                    .addComponent(jbtn6)
                    .addComponent(jbtn7)
                    .addComponent(jbtn8)
                    .addComponent(jbtn9)
                    .addComponent(jbtn0)
                    .addComponent(jbtnDelete))
                .addGap(42, 42, 42))
        );

        jpKeyPadLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jbtn0, jbtn1, jbtn2, jbtn3, jbtn4, jbtn5, jbtn6, jbtn7, jbtn8, jbtn9, jbtnDelete});
*/
        
        jbtnVirtualKeyboard.setText("Virtual Keybord");
        jbtnVirtualKeyboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                virtualKeyBordActionPerformed(evt);
            }
        });
        
        javax.swing.GroupLayout jpKeyPadLayout = new javax.swing.GroupLayout(jpKeyPad);
        jpKeyPad.setLayout(jpKeyPadLayout);
        jpKeyPadLayout.setHorizontalGroup(
            jpKeyPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpKeyPadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jbtnVirtualKeyboard, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jpKeyPadLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jbtnVirtualKeyboard});

        jpKeyPadLayout.setVerticalGroup(
            jpKeyPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpKeyPadLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jpKeyPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtnVirtualKeyboard, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42))
        );

        jpKeyPadLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jbtnVirtualKeyboard});

        jlblStatus.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jlblStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblStatus.setText("Please Wait we are verifying your number ....");

        javax.swing.GroupLayout jpProgressStatusLayout = new javax.swing.GroupLayout(jpProgressStatus);
        jpProgressStatus.setLayout(jpProgressStatusLayout);
        jpProgressStatusLayout.setHorizontalGroup(
            jpProgressStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpProgressStatusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpBusyBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jpProgressStatusLayout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addComponent(jlblStatus)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpProgressStatusLayout.setVerticalGroup(
            jpProgressStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpProgressStatusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlblStatus)
                .addGap(26, 26, 26)
                .addComponent(jpBusyBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jTextField1.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	virtualKeyBordActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        //jLabel1.setText("Enter Your 10 Digit Mobile number Below");
        jLabel1.setText("Please enter merchant app id");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jlblError.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jlblError.setForeground(new java.awt.Color(250, 5, 5));
        jlblError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jpKeyPad, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlblError, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    /*.addComponent(jpKeyPad, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)*/
                    .addComponent(jbVerify, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpProgressStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 519, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jpKeyPad, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jlblError, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jpProgressStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                /*.addComponent(jpKeyPad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)*/
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbVerify, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jpProgressStatus.setVisible(false);
        jpProgressStatus.setVisible(false);
        jTextField1.getAccessibleContext().setAccessibleName("");
    }// </editor-fold>//GEN-END:initComponents

    private void jbVerifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbVerifyActionPerformed
        // TODO add your handling code here
        jlblError.setText("");
        //String sellerPhoneNumber = jTextField1.getText();
        String merchantId = jTextField1.getText();
        if(merchantId != null && !merchantId.isEmpty()){
            //if(isNumber(merchantId.toCharArray()) && (sellerPhoneNumber.length() == 10)){
        	if(merchantId.length() == 8){
                jlblError.setText("");
                hideAndSeek();
            }else{
//                jlblError.setText("Invalid Number");
                jlblError.setText("Invalid App ID");
            }
        }else{
//            jlblError.setText("Should Not be Empty");
            //jlblError.setText("please enter Merchant Mobile number");
        	jlblError.setText("please enter valid merchant app id");
        }
             
    }//GEN-LAST:event_jbVerifyActionPerformed
    
    private void virtualKeyBordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn0ActionPerformed
        try {
			Runtime.getRuntime().exec("cmd /c osk");
		} catch (IOException e) {
		}
    }

    private void jbtn0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn0ActionPerformed
        // TODO add your handling code here:
        String prevNumber = jTextField1.getText();
        //System.out.println("evt.getActionCommand() + " + evt.getActionCommand());
        String currentNumber = "";
        if(!evt.getActionCommand().equals("Del")){
            currentNumber = prevNumber + evt.getActionCommand();
            //System.out.println("evt.getActionCommand() + " + evt.getActionCommand());
        }else{
            jTextField1.setText("");
        }
        if ((currentNumber.length() < 11) && numberValidation(currentNumber)) {
            jTextField1.setText(currentNumber);
        }
    }//GEN-LAST:event_jbtn0ActionPerformed

   private boolean numberValidation(String number){
       boolean isNumber = false;
       try {
           for(int i=0; i < number.length(); i++){
               Integer.parseInt(number.charAt(i)+"");
           }
           isNumber = true;
       } catch (Exception e) {
           isNumber = false;
       }
       return isNumber;
   } 
    
    private boolean isNumber(char[] number){
        boolean isNumber = false;
        
        try{
            for(int i = 0; i < number.length; i++)
                Integer.parseInt("" + number[i]);
            isNumber = true;
        }catch(Exception e){
            //e.printStackTrace();
            isNumber = false;
        }
        //System.out.println("is number " + isNumber);
        
        return isNumber;
    }
    
    private void hideAndSeek(){
        if(!jpProgressStatus.isVisible()){
            jpProgressStatus.setVisible(true);
            jpBusyBar.setIndeterminate(true);
            jbVerify.setText("Cancel");
            //start process
            callStatus = "init";
            Settings settings = createSetting();
            
            //callBackend(Constants.API_NUMBER_CHECK, settings);
            callBackend(Constants.API_MERCHANT_NUMBER_CHECK, settings);
        }else{
            jpProgressStatus.setVisible(false);
            jpBusyBar.setIndeterminate(false);
            jbVerify.setText("Verify");
            callStatus = "cancel";
        }
    }

    private void callBackend(String API, Settings settings){
        //Comment"data", data);
        params = new ArrayList<NameValuePair>();
       
//        params.add(new BasicNameValuePair("phone", jTextField1.getText()));
//        params.add(new BasicNameValuePair("device_id", getPCSerialNumber()));
        params.add(new BasicNameValuePair("app_id", jTextField1.getText()));
        params.add(new BasicNameValuePair("device_id", getPCSerialNumber()));
        
        mNetworkAsyncTask = new NetworkAsyncTask(settings, API, NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);

        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                //CommentTAG, (String) obj);
                JsonParser jsonParser = new JsonParser((String) obj);
//                System.out.println("obj: "+obj);
                if (jsonParser.isHavingSuccess()){
                    if(jbVerify.getText().equalsIgnoreCase("Cancel")){
                        putFileData("seller.dat", (String) obj);
                        mainForm.initMerchantDetails((String) obj);
                        String tid = jsonParser.getTID();
                        saveTID_in_File(tid);
                        //setAmountPanel();
                        //setTransactionPanel();
                        mainForm.callTransactionFunction(mainForm.tnsMod);
                    }
                }else{
                    jlblError.setText("Merchant Not Found");
                    hideAndSeek();
                }
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
                String message = "";
                if(responsePhrase != null && (responsePhrase.contains(Constants.PRODUCTION_IP)
                		|| responsePhrase.contains("No route to host"))){
                	jpProgressStatus.setVisible(false);
                	JOptionPane.showMessageDialog(mainForm, "please check your network connection");                	
                }
                else if (responsePhrase != null) {
                	message = responsePhrase;
                	jlblError.setText(message);
                }
                jpProgressStatus.setVisible(true);
                hideAndSeek();
            }
        });

        mNetworkAsyncTask.start();
    }
    
    private void setAmountPanel(){
      jpAmountPanel = new AmountPanel(mainForm); 
 		jpBasePanel.removeAll();
        jpBasePanel.add(jpAmountPanel);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    private void setTransactionPanel(){
//    	mainForm.selectedTellerCode = tellers.get(jcbTellers.getSelectedIndex()).getTeller_code();
    	jpTransaction = new TransactionPanel(mainForm);
    	jpBasePanel.removeAll();
        jpTransaction.setAmount(mainForm.amount);
        jpBasePanel.add(jpTransaction);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    public void initMerchantDetails(String response){
        
        //String response = getFileData("seller.dat");

        //System.out.println("file data : " + response);

        JsonParser jp = new JsonParser(response);//seller.sellerFile);

        if(jp.isHavingSuccess()){
            merchant = jp.parseToMerchant();
            
            tellers = merchant.getTellers();
//            System.out.println("tell: "+tellers.get(0).getTeller_code());
        }
    }
    
    private javax.swing.JPanel jpAmountPanel;
    private TransactionPanel jpTransaction;
    
    private boolean putFileData(String fileName, String data) {
        boolean result= false;
        OutputStream os = null;
        try {
            ClassLoader cl = getClass().getClassLoader();
            URL imageURL = cl.getResource(fileName);//getResourceAsStream(fileName);//"resources/seller.json");
            //byte buf[] = data.getBytes();
            os = new FileOutputStream(fileName);//imageURL.getFile());
            
            os.write(Utils.encrypt(data));
            result = true;
            
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (Exception ex) {
        } finally {
            try {
                os.close();
            } catch (IOException ex) {
            }
        }

        return result;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JButton jbVerify;
    private javax.swing.JButton jbtn0;
    private javax.swing.JButton jbtn1;
    private javax.swing.JButton jbtn2;
    private javax.swing.JButton jbtn3;
    private javax.swing.JButton jbtn4;
    private javax.swing.JButton jbtn5;
    private javax.swing.JButton jbtn6;
    private javax.swing.JButton jbtn7;
    private javax.swing.JButton jbtn8;
    private javax.swing.JButton jbtn9;
    private javax.swing.JButton jbtnDelete;
    private javax.swing.JButton jbtnVirtualKeyboard;
    private javax.swing.JLabel jlblError;
    private javax.swing.JLabel jlblStatus;
    private javax.swing.JProgressBar jpBusyBar;
    private javax.swing.JPanel jpKeyPad;
    private javax.swing.JPanel jpProgressStatus;
    // End of variables declaration//GEN-END:variables

    private Settings createSetting() {
        return mainForm.mSettings;
    }
    private String getPCSerialNumber(){
    	String serialnumber = "";
    	try {
			Process p = Runtime.getRuntime().exec(new String[]{"wmic","bios","get","serialnumber"});
			p.getOutputStream().close();
			Scanner s = new Scanner(p.getInputStream());
			String pro = s.next();
			serialnumber = s.next();
		} catch (IOException e) {
		}
    	return serialnumber;
    }
    private void saveTID_in_File(String tid){
		Properties prop = new Properties();
		OutputStream output = null;
		try {
			String formatedTid = getFormatedTid(tid);
			File f = new File(Constants.TID_FILE_NAME);
			if(!f.canWrite())
				f.setWritable(true);
			output = new FileOutputStream(f);
			// set the properties value
			prop.setProperty(tid, formatedTid);

			// save properties to project root folder
			prop.store(output, null);
			//f.setReadOnly();
			
		} catch (IOException io) {
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String getFormatedTid(String tid) {
//		Integer currentTime = (int)new Date().getTime();
		//long txnIDNew = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
		//String transactionId = ""+ Long.toString(txnIDNew);
		
		Calendar calender = Calendar.getInstance();
		final Integer julianYear = calender.get(Calendar.YEAR);
		final Integer julianDay = calender.get(Calendar.DAY_OF_YEAR);
		final Integer julianHrD = calender.get(Calendar.HOUR_OF_DAY);
		final Integer julianMnt = calender.get(Calendar.MINUTE);
		final Integer julianSec = calender.get(Calendar.SECOND);
		
		String yr = julianYear.toString();
		String days = julianDay.toString();
		String hrs = julianHrD.toString();
		String mnts = julianMnt.toString();
		String sec = julianSec.toString();
		if(days != null && !days.isEmpty()){
			if(days.length()<=3){
				if(days.length()==1)
					days = "00"+days;
				if(days.length()==2)
					days = "0"+days;
			}
		}
		if(hrs != null && !hrs.isEmpty()){
			if(hrs.length()!=2){
				if(hrs.length()==1)
					hrs = "0"+hrs;
			}
		}
		if(mnts != null && !mnts.isEmpty()){
			if(mnts.length()!=2){
				if(mnts.length()==1)
					mnts = "0"+mnts;
			}
		}
		if(sec != null && !sec.isEmpty()){
			if(sec.length()!=2){
				if(sec.length()==1)
					sec = "0"+sec;
			}
		}
		
		//String formatedNumber = yr.charAt(3)+"_"+julianDay+"_"+julianHrD+"_"+julianMnt+"_"+julianSec;
		String formatedNumber = yr.charAt(3)+""+days+""+hrs+""+mnts+""+sec;
		String formatedId = Constants.SELLER_IDENTIFIER+tid+formatedNumber;
//		System.out.println("formatedNumber: "+formatedNumber);
//		System.out.println("formatedId: "+formatedId);
		return formatedId;
	}
}
