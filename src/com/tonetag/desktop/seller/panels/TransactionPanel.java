
package com.tonetag.desktop.seller.panels;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import com.sun.jmx.snmp.tasks.Task;
import com.tonetag.desktop.seller.Seller;
import com.tonetag.desktop.seller.bean.Bill;
import com.tonetag.desktop.seller.bean.Merchant;
import com.tonetag.desktop.seller.bean.Settings;
import com.tonetag.desktop.seller.bean.Teller;
import com.tonetag.desktop.seller.constants.Applib;
import com.tonetag.desktop.seller.constants.Constants;
import com.tonetag.desktop.seller.frames.DialogFrame;
import com.tonetag.desktop.seller.implementations.FileLogger;
import com.tonetag.desktop.seller.interfaces.Logger;
import com.tonetag.desktop.seller.tasks.JsonParser;
import com.tonetag.desktop.seller.tasks.NetworkAsyncTask;
import com.tonetag.desktop.seller.tasks.Utils;
import com.tonetag.desktop.tone.SoundPlayer;
import com.tonetag.desktop.tone.SoundRecorder;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 *
 * @author dev1
 */
public class TransactionPanel extends javax.swing.JPanel {
    static {
	/*String arch = System.getProperty("sun.arch.data.model");
        //System.out.println("Arch " + arch);
        if (arch.equals("32")) {
            System.loadLibrary("ToneTagLX86");
        }else{
            System.loadLibrary("ToneTagLX64");
        }*/
        
        if(Utils.isUnix()){
            if(Utils.isX86()){
                System.loadLibrary("ToneTagLX86");
            }
            if(Utils.isX64()){
                System.loadLibrary("ToneTagLX64");
            }
        }
        
        if(Utils.isWindows()){
           // System.loadLibrary("ToneTagWin");
//        	new Applib().loadLib();
        	System.load(new File(Constants.TONETAG_DLL).getAbsolutePath());
//        	System.load("C:\\tonetag\\lib\\libToneTagWinx64.dll");
            //System.load("C:\\tonetag\\lib\\libToneTagWin.dll");
        }
        if(Utils.isMac()){
        	System.out.println("before call mac");
            System.loadLibrary("ToneTagMac");
            System.out.println("after call mac");
        }
    }

    private Seller mainForm;
    
    private String amount = "0.00";
    
    private Bill bill;
    private Logger mLogger;
    private Merchant merchant;
    private List<Teller> tellers; 
    private String selectedTellerCode;
    private javax.swing.JPanel jpBasePanel;
    
    private static final int TXN_INITIALISATION = 0;
    
    private static final int TXN_AUTHENTICATION = 1;
    
    private int mode;
    
    private boolean encryptedToneReceived = false;
    private boolean trackIdReceived = false;

    private int whichApiCall = 1;

    private static final int TXN_API_CALL = 2;
    private static final int OTP_API_CALL = 1;
    
    private static final int TXN_ONE_WAY = 1;
    private static final int TXN_TWO_WAY = 0;
    
    public final static int GET = 1;
    public final static int POST = 2;

    private int txnFlow = 0;
    private char customerIdentificationFlag = '0';

    private static final char BIC_CARD_IDENTIFIER = 'C';
    private static final char BIC_WALLET_IDENTIFIER = 'W';
    private static final char BIC_OTHERS_IDENTIFIER = 'O';
    private static final char BIC_BANK_IDENTIFIER = 'B';

    private static final char NULL_MODE_OF_PAYMENT = 'N';

    public static final int TRANSACTION_RESULT_SUCCESS = 1;
    public static final int TRANSACTION_RESULT_FAILED = 0;
    
    private char modeOfPayment = 'N';
    private String track_ID = null;
    private String bic;
    private String cAppId;
    private String tokenIDOrPhoneNum = "";
    //private String tellerCode;
    private String receivedString;
    private List<NameValuePair> params = null;
    private NetworkAsyncTask mNetworkAsyncTask;
    private String strTxnIdOnlineUpdated;
    
    private Timer timer;
    private TimerTask timerTask;
    
    
    /**
     * Creates new form TransactionPanel
     */
    public TransactionPanel(Seller mainForm) {
        this.mainForm = mainForm;
        this.tellers = mainForm.tellers;
        this.merchant = mainForm.merchant;
        //this.selectedTellerCode = mainForm.selectedTellerCode;//"C34XLN";
        this.selectedTellerCode = this.tellers.get(0).getTeller_code();
        //System.out.println("this.selectedTellerCode : " + mainForm.selectedTellerCode);
        this.jpBasePanel = mainForm.jpBasePanel;
        //this.micDriver = mainForm.mSettings.getMicMixer();
        this.speakerDriver = mainForm.mSettings.getSpeakerMixer();
        try{
            mLogger = new FileLogger();
        }catch(IOException ioe){
            System.out.println("Log file Exception : " + ioe.getLocalizedMessage());
        }
        /*mSoundPlayer = new SoundPlayer(speakerDriver);
        mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
        mSoundRecorder = new SoundRecorder(micDriver);
        mSoundRecorder.setOnDataFoundListener(new OnSoundDataReceived());*/
    }
    
    public void setAmount(String amount){
        this.amount = amount;
        initComponents();
        /*   Getting Txn Id for Online Txn */

        List<NameValuePair> paramValue = new ArrayList<NameValuePair>();
        paramValue.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));
        paramValue.add(new BasicNameValuePair("amount", amount));
        // paramValue.add(new BasicNameValuePair("bic", bic));
        // paramValue.add(new BasicNameValuePair("c_app_id", cAppId));
        paramValue.add(new BasicNameValuePair("teller_code", selectedTellerCode));
        paramValue.add(new BasicNameValuePair("demo_txn", "false"));

        String apiTxnIdReq = Constants.API_ONLINE_TXN_ID;
//        if(!conDetect.isConnectingToInternet())
//        {
//            L.showToast(this, getString(R.string.error_no_connectivity_to_server));
//            return;
//        }
        try{
//            sendOnlineDetailsAndCheckTxnToBackend(paramValue, apiTxnIdReq, "TxnFlag");
           // sendOnlineDetails(paramValue, apiTxnIdReq, "TxnFlag");
        }catch(Exception e){
            mLogger.writeEntry(e);
            
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlblProcessTxn = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jlblAmount = new javax.swing.JLabel();
        jpTransaction = new javax.swing.JPanel();
        jbProceed = new javax.swing.JButton();
        jpTransactionProgress = new javax.swing.JPanel();
        jlblProgressText = new javax.swing.JLabel();
        pbTransactionProgress = new javax.swing.JProgressBar();
        jbCancel = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(600, 600));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jlblProcessTxn.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jlblProcessTxn.setText("<html>Processing Transaction For</html>");
        add(jlblProcessTxn, new org.netbeans.lib.awtextra.AbsoluteConstraints(139, 12, -1, 59));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jLabel1.setText("Rs");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(223, 89, 60, 56));

        jlblAmount.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jlblAmount.setText(this.amount);
        add(jlblAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(289, 89, 227, 56));

        jpTransaction.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jbProceed.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jbProceed.setText("Proceed");
        jbProceed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbProceedActionPerformed(evt);
            }
        });
        jpTransaction.add(jbProceed, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, 552, 76));

        add(jpTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 382, 576, -1));

        jlblProgressText.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jlblProgressText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblProgressText.setText("Progress");
        jlblProgressText.setFocusCycleRoot(true);

        javax.swing.GroupLayout jpTransactionProgressLayout = new javax.swing.GroupLayout(jpTransactionProgress);
        jpTransactionProgress.setLayout(jpTransactionProgressLayout);
        jpTransactionProgressLayout.setHorizontalGroup(
            jpTransactionProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpTransactionProgressLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpTransactionProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pbTransactionProgress, javax.swing.GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)
                    .addComponent(jlblProgressText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpTransactionProgressLayout.setVerticalGroup(
            jpTransactionProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpTransactionProgressLayout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(jlblProgressText)
                .addGap(18, 18, 18)
                .addComponent(pbTransactionProgress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jpTransactionProgress, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 382, 576, 90));

        jbCancel.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jbCancel.setText("Cancel");
        jbCancel.setToolTipText("Click to Cancel transaction");
        jbCancel.setFocusCycleRoot(true);
        jbCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelActionPerformed(evt);
            }
        });
        add(jbCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 512, 576, 76));
    }// </editor-fold>//GEN-END:initComponents
    
    private void windowClose(){
    	if(this.mainForm.track_Ref_ID != null && !this.mainForm.track_Ref_ID.isEmpty()){
    		int confirm = JOptionPane.showOptionDialog(
                    new Seller(), "Do you want cancel transaction", "Exit Confirmation", JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE, null, null, null);
               if (confirm == 0) {
            	  callCancelTransProcess(this.mainForm.track_Ref_ID);
            	  System.exit(0);
               }
    	} else
    		System.exit(0);
    }
    private void jbProceedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbProceedActionPerformed
        // TODO add your handling code here:
        
        if(speakerDriver != null){
            amount = jlblAmount.getText();
            startPlaySound(SoundRecorder.STRING_RECORD_MODE, amount);
            // initialise flags
            encryptedToneReceived = false;
            trackIdReceived = false;

            // Start transaction process here
            // triggerToResultPanel();
            whichApiCall = OTP_API_CALL;
            mode = TXN_INITIALISATION;
//            stopRecorder();
//            startRecorder(SoundRecorder.STRING_RECORD_MODE, SoundRecorder.SENDER_CUSTOMER);
            hideCancelAndSeek();
//            hideAndSeek();
        }else{
            //System.out.println("Device not selected");
        }
        

    }//GEN-LAST:event_jbProceedActionPerformed

//    private void playTone(String data){
//        if(speakerDriver != null){
//            mSoundPlayer = new SoundPlayer(speakerDriver);
//            mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
//            /*new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    createDialog("Sending String Over Sound...");
//                }
//            }).start();*/
//
//            mSoundPlayer.sendString(data);
//        }
//    }
    
//    private void startRecordingTone(){
//        if(micDriver != null){
//            mSoundRecorder = new SoundRecorder(micDriver);
//            mSoundRecorder.setOnDataFoundListener(new OnSoundDataReceived());
//            /*new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    createDialog("Receiver is Running...");
//                }
//            }).start();*/
//            mSoundRecorder.startRecording(SoundRecorder.STRING_RECORD_MODE, 
//                    SoundRecorder.SENDER_CUSTOMER, false, 3000, 16);
//        }
//    }
    
    private void hideAndSeek(){
        if(jpTransaction.isVisible()){
            jpTransaction.setVisible(false);
            pbTransactionProgress.setIndeterminate(true);
            jlblProgressText.setText("connecting server, please wait...");
            jpTransactionProgress.setVisible(true);
        }else{
        	if(!jbCancel.isVisible())
        		jbCancel.setVisible(true);
            jpTransaction.setVisible(true);
            pbTransactionProgress.setIndeterminate(false);
            jpTransactionProgress.setVisible(false);
        }
    }
    
    private void hideCancelAndSeek(){
        if(jpTransaction.isVisible()){
        	jbCancel.setVisible(false);
            jpTransaction.setVisible(false);
            pbTransactionProgress.setIndeterminate(true);
            jlblProgressText.setText("connecting server, please wait...");
            jpTransactionProgress.setVisible(true);
            
        }else{
        	jbCancel.setVisible(true);
            jpTransaction.setVisible(true);
            pbTransactionProgress.setIndeterminate(false);
            jpTransactionProgress.setVisible(false);
        }
    }

    
    private void apiRequestToSendOTP(final int apiCall, final String s){
        new Thread(new Runnable() {
            @Override
            public void run() {
                /*try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                if (apiCall == OTP_API_CALL) {
                    //System.out.println("OTP Call For " + s);
                    //CommentTAG, "OTP Call For " + s);

                    //bic = s.substring(s.length()-4,s.length());
                    //char bicIdentifier = bic.charAt(0);
                    //modeOfPayment = bicIdentifier;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // TODO change progress status here
                            //ivProgressStep.setImageResource(R.drawable.p_layer_two);
                            switch (modeOfPayment){
                                case 'C':
                                    //for online card transaction
                                    if(bic.equalsIgnoreCase("C420")){
                                        //System.out.println("bic : "+ bic);
                                        isOnlineCardMode = true;
                                        cAppId = s.substring(0, 8);
//                                        new Thread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                SoundPlayer onlineCardSoundPlayer = new SoundPlayer(speakerDriver);
//                                                onlineCardSoundPlayer.sendString(strTxnIdOnlineUpdated + amount);
//                                            }
//                                        }).start();
                                        sendInvoice();
                                        //startPlayer(SoundRecorder.STRING_RECORD_MODE, (strTxnIdOnlineUpdated + amount));
                                        //mSoundPlayer.sendString(strTxnIdOnlineUpdated + amount);

                                        startTimerTxnStatusCheck();
                                    }else{
                                        //Api call for offline card transaction
                                        //System.out.println("bic : "+ bic);
                                        callBackend(s, apiCall, Constants.API_CARD_TRANSACTION);
                                    }
                                    break;
                                case 'W':
                                    txnFlow = Integer.parseInt(s.charAt(10)+"");
                                    callBackend(s, apiCall, Constants.API_WALLET_TRANSACTION);
                                    // API call for wallet transaction
                                    break;
                                case 'B':
                                    // API call for Bank Transaction
                                    break;
                                case 'O':
                                    //API call for Other Transaction
                                    break;
                                default:
                                    //L.e(TAG, " somthing wrong");
                                    break;
                                }
                            }
                        }).start();

                    }
                if (apiCall == TXN_API_CALL){
                    encryptedToneReceived = true;
                    //System.out.println("TXN Call For " + s);
                    //trackIdReceived = false;
                    //Log.e(TAG, "TXN Call For " + s);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (encryptedToneReceived == true && trackIdReceived == true) {
                                // TODO change progress status here
                                //ivProgressStep.setImageResource(R.drawable.p_layer_three);
                                //showLoadingDialog(getResources().getString(R.string.process_msg_five));
                                switch (modeOfPayment) {
                                    case 'C':
                                        //for online card transaction
//                                        if(bic.equalsIgnoreCase("C420")){
//                                            System.out.println("");
//                                    
//                                            cAppId = s.substring(0, 8);
//
//                                            mSoundPlayer.sendString(strTxnIdOnlineUpdated + amount);
//
//                                            startTimerTxnStatusCheck();
//                                        }else{
                                        //Api call for offline card transaction
                                            callBackend(s, apiCall, Constants.API_CARD_AUTHENTICATION);
                                        //}
                                        break;
                                    case 'W':
                                        // API call for wallet transaction
                                        callBackend(s, apiCall, Constants.API_WALLET_AUTHENTICATION);
                                        break;
                                    case 'B':
                                        // API call for Bank Transaction
                                        break;
                                    case 'O':
                                        //API call for Other Transaction
                                        break;
                                    default:
                                        //L.e(TAG, " something wrong");
                                        break;
                                }
                            }

                        }
                    }).start();


                    // if customer used wallet
                    /*if (mDialog != null) {
                        mDialog.cancel();
                    }*/
                    /*Log.d(TAG, "!data received: " + s);
                    Intent intent = new Intent(mContext, CardDetailActivity.class);
                    intent.putExtra("mode", "wallet");
                    intent.putExtra("amount", Double.parseDouble(amount));
                    startActivity(intent);*/

                }
            }
        }).start();
    }
    
    private void callBackend(final String data, final int apiCall, final String API){

        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {*/
                txnProcess(data, apiCall, API);
            /*}
        });*/

    }
    
    private void txnProcess(String data, final int apiCall, String API){
        //Comment"data", data);
        params = new ArrayList<NameValuePair>();
        //System.out.println("txnProcess Called");
        if (apiCall == OTP_API_CALL){

            if (modeOfPayment == BIC_CARD_IDENTIFIER){
                tokenIDOrPhoneNum = data.substring(0,data.length()-6);
                params.add(new BasicNameValuePair("token_id", tokenIDOrPhoneNum));
                params.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));

            }else if (modeOfPayment == BIC_WALLET_IDENTIFIER){
                tokenIDOrPhoneNum = data.substring(0,data.length()-6);
                //cAppId = data.substring(data.length()-12,data.length()-4);

                params.add(new BasicNameValuePair("wallet_id", tokenIDOrPhoneNum));
                params.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));
                //params.add(new BasicNameValuePair("c_app_id", cAppId));
            }
            params.add(new BasicNameValuePair("bic", bic));
            params.add(new BasicNameValuePair("amount", amount));

            //L.e(TAG, "tokenIDOrPhoneNum : " + tokenIDOrPhoneNum  + ", m_app_id: "+ merchant.getApp_id() + ", bic: " + bic + ", amount: " + amount);

        }
        if (apiCall == TXN_API_CALL){
            if (txnFlow == TXN_TWO_WAY) {
                cAppId = data.substring(data.length() - 8, data.length());
                //L.e(TAG, "track_ID" + track_ID);
                params.add(new BasicNameValuePair("data", data));
            }
            //cAppId = data.substring(data.length()-8,data.length());
            //L.e(TAG, "track_ID" + track_ID);
            //params.add(new BasicNameValuePair("data", data));
            params.add(new BasicNameValuePair("track_id", track_ID));
            params.add(new BasicNameValuePair("token_id", tokenIDOrPhoneNum));
            params.add(new BasicNameValuePair("teller_code", selectedTellerCode));

            if (modeOfPayment == BIC_WALLET_IDENTIFIER){
                params.add(new BasicNameValuePair("m_app_id", merchant.getApp_id()));
                params.add(new BasicNameValuePair("c_app_id", cAppId));
            }
            //System.out.println("Teller : " + selectedTellerCode);

            /*L.e(TAG, "data : " + data + ", c_app_id: " + cAppId + ", m_app_id: " +
                    merchant.getApp_id() + ", tokenIDOrPhoneNum : " + tokenIDOrPhoneNum  +
                    ", track_id: " + track_ID + ", teller_code :" + tellerCode);*/
        }

        /*if (mDialog != null){
            mDialog.setMessage(getResources().getString(R.string.get_txn_info));
        }*/

        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), API, NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);

        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                //CommentTAG, (String) obj);
                JsonParser jsonParser = new JsonParser((String) obj);

                if (apiCall == OTP_API_CALL){

                    /*Merchant merchant = db.getUser();
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    String kycStatus = jsonParser.getTagKycStatus();
                    boolean isKycUploaded = jsonParser.isKycUploaded();*/

                    if (jsonParser.isHavingSuccess()){
                        track_ID = jsonParser.getTrackId();

                        /*merchant.setKyc_status(kycStatus);
                        merchant.setKyc_uploaded(isKycUploaded);
                        db.updateMerchantDetail(merchant);
                        db.close();

                        editor.putString(PREF_KEY_KYC, merchant.getKyc_status());
                        editor.putBoolean(PREF_KEY_KYC_UPLOADED, merchant.getKyc_uploaded());

                        editor.commit();*/

                        /*if (whichApiCall == OTP_API_CALL){
                            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                            profileSet = true;
                            mSoundPlayer.setDeviceVolume(volLevel);
                            mSoundPlayer.sendAmount(Float.parseFloat(amount));
                        }*/
                        //encryptedToneReceived = false;
                        if(txnFlow == TXN_ONE_WAY){
                            //System.out.println("One way found at api call");
                            whichApiCall = TXN_API_CALL;
                            encryptedToneReceived = true;
                        }
                        trackIdReceived = true;
                        isTxnFailed = false;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                jlblProgressText.setText("Customer Identified...");
                            }
                        }).start();
                        
                        if (encryptedToneReceived == true){
                            //System.out.println("encryptedToneReceived = true");
                            apiRequestToSendOTP(whichApiCall, receivedString);
                        }
                        
                    }else{
                        /*merchant.setKyc_status(kycStatus);
                        merchant.setKyc_uploaded(isKycUploaded);
                        db.updateMerchantDetail(merchant);
                        db.close();

                        editor.putString(PREF_KEY_KYC, merchant.getKyc_status());
                        editor.putBoolean(PREF_KEY_KYC_UPLOADED, merchant.getKyc_uploaded());

                        editor.commit();*/

                        //L.e(TAG, jsonParser.getMessage());
                        isTxnFailed = true;
                        stopRecorder();
                        //System.out.println("Txn init faild ");
                        genBill("", TRANSACTION_RESULT_FAILED, jsonParser.getMessage());
                    }
                }
                if (apiCall == TXN_API_CALL){
                    if (jsonParser.isHavingSuccess()){
                        //Log.e("hi", "i called");
                        // TODO change status image here
                        //ivProgressStep.setImageResource(R.drawable.p_layer_four);
                        modeOfPayment = NULL_MODE_OF_PAYMENT;
                        isTxnFailed = false;
                        genBill(track_ID, TRANSACTION_RESULT_SUCCESS,jsonParser.getMessage());
                    }else{
                        isTxnFailed = true;
                        genBill(track_ID, TRANSACTION_RESULT_FAILED, jsonParser.getMessage());
                        //finish();
                        //L.e(TAG, jsonParser.getMessage());
                    }
                }
                /*if (!jsonParser.isHavingError()) {
                    Map<String, String> map = jsonParser.getTxnValues();
                    String bic = map.get("bic");
                    String walletSpec = map.get("walletspec");
                    //L.e(TAG, "bic : " + bic  + " walletspec : "+ walletSpec);
                    genBill("123456");
                }else{
                    //L.e(TAG, "error ");
                }*/
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
                String message = "";
                if (responsePhrase != null) message = responsePhrase;
                if(track_ID == null || track_ID.isEmpty()) track_ID = "";
                stopPlayer();
                stopRecorder();
                genBill(track_ID, TRANSACTION_RESULT_FAILED, message);
                //L.e(TAG, responsePhrase);
                // TODO show Alert dialog here
                //showAlertDialog(ProcessActivity.this, null, message, null, HomeActivity.class);

            }
        });

        mNetworkAsyncTask.start();
    }
    
    private void genBill(String txn_id, int result, String message){
        
        // invoke bill here
        bill = new Bill();
        bill.status = result;
        bill.message = message;
        bill.txnNumber = txn_id;
        bill.amount = amount;
        triggerToResultPanel();
        /*Intent intent = new Intent(mContext, ReceiptActivity.class);
        intent.putExtra("mode", "wallet");
        intent.putExtra("amount", Double.parseDouble(amount));
        intent.putExtra("result", result);
        intent.putExtra("txn_id", txn_id);
        intent.putExtra("msg", message);
        startActivity(intent);
        finish();*/
    }
    
    private void jbCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCancelActionPerformed
        // TODO add your handling code here:
    	
    	System.exit(0);
        // Immidiate cancel of transaction
        stopRecorder();
        stopPlayer();
        try{
            if(timer != null)
            	timer.cancel();
        }catch(Exception e){
            mLogger.writeEntry(e);
            
        }
        //triggerToMainPanel();
        
    }//GEN-LAST:event_jbCancelActionPerformed

    private String speakerDriver = null;
    private String micDriver = null;
    
    private void startPlayer(int type, String data){
        /*if(mSoundPlayer != null){
            mSoundPlayer = new SoundPlayer();
        }else{
            mSoundPlayer.setOnPlaybackFinishedListener(null);
        }*/
        
        if(speakerDriver != null){
            mSoundPlayer = new SoundPlayer(speakerDriver);
            mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    createDialog("Sending String Over Sound...");
                }
            }).start();*/

            if(type == SoundRecorder.AMOUNT_RECORD_MODE){
                //System.out.println("Amout Playing");
                mSoundPlayer.sendAmount(Float.parseFloat(data));
            }
            if(type == SoundRecorder.STRING_RECORD_MODE){
                mSoundPlayer.sendString(data);
            }
        }
        //System.out.println("Player started");
        /*if(type == SoundRecorder.AMOUNT_RECORD_MODE){
            mSoundPlayer.sendAmount(Float.parseFloat(data));
        }
        if(type == SoundRecorder.STRING_RECORD_MODE){
            mSoundPlayer.sendString(data);
        }*/
        
    }
    
    private void startPlaySound(int type, String amount){
        if(speakerDriver != null){
        	this.amount = amount;
        	String tid = merchant.getTid();
            String refCode = getFormatedTid(tid);
//            System.out.println("refCode: "+refCode);
            saveTID_in_File(tid, refCode);
            amount = refCode+amount;
//            System.out.println("amnt: "+amount);
            mSoundPlayer = new SoundPlayer(speakerDriver);
//            mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackFinished());
            mSoundPlayer.setOnPlaybackFinishedListener(new OnSoundDataPlaybackOnlineCardMode());
            if(type == SoundRecorder.AMOUNT_RECORD_MODE){
                mSoundPlayer.sendAmount(Float.parseFloat(amount));
            }
            if(type == SoundRecorder.STRING_RECORD_MODE){
            	
//                System.out.println("amnt data: "+amount);
//                mSoundPlayer.sendString(amount);
                mSoundPlayer.send30ByteString(amount);
            }
        }
    }
    
    private void startSoundRecorder(int type, int from){
    	
        if(micDriver != null){
			try {
				mSoundRecorder = new SoundRecorder(micDriver);
				mSoundRecorder.setOnDataFoundListener(new OnDataReceived());
				mSoundRecorder.startRecording(type, from, false, 3000, 0);

			} catch (Exception e) {
				System.out.println("==========");
				e.printStackTrace();
			}
        }
    }
    
    private void startRecorder(int type, int from){
        /*if(mSoundRecorder != null){
            mSoundRecorder = new SoundRecorder();
        }else{
            mSoundRecorder.setOnDataFoundListener(null);
        }*/
        
        //System.out.println("Recorder started");
        if(micDriver != null){
            mSoundRecorder = new SoundRecorder(micDriver);
            mSoundRecorder.setOnDataFoundListener(new OnSoundDataReceived());
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    createDialog("Receiver is Running...");
                }
            }).start();*/
            /*mSoundRecorder.startRecording(SoundRecorder.STRING_RECORD_MODE, 
                    SoundRecorder.SENDER_CUSTOMER, false, 3000, 16);*/
            mSoundRecorder.startRecording(type, from, false, 3000, 0);
        }
        
        //mSoundRecorder.startRecording(type, from, false, 3000, 0);
       
        
    }
    
    /*private void stopRecorder(){
        if(mSoundRecorder != null){
            if(mSoundRecorder.isRecordingOn()){
                mSoundRecorder.stopRecording();
            }
        }
    }*/
    
    private boolean isOnlineCardMode = false;
    
    private class OnSoundDataPlaybackOnlineCardMode implements SoundPlayer.OnPlaybackFinishedListener{

        @Override
        public void onPlaybackFinished() {
            
            	 callOnLineTransProcess(Constants.API_ONLINE_TRANSACTION);
            	 
                 //startSoundRecorder(SoundRecorder.STRING_RECORD_MODE, SoundRecorder.SENDER_CUSTOMER);
        }
    }
    
    private class OnSoundDataPlaybackFinished implements SoundPlayer.OnPlaybackFinishedListener{

        @Override
        public void onPlaybackFinished() {
            
            //System.out.println("onPlaybackFinished");
            
            if(!isOnlineCardMode){
                mode = TXN_AUTHENTICATION;
                whichApiCall = TXN_API_CALL;
                jlblProgressText.setText("Generating OTP for Customer");
                if(txnFlow == TXN_TWO_WAY) {
                    startRecorder(SoundRecorder.TXN_STRING_RECORD_MODE, SoundRecorder.SENDER_CUSTOMER);
                }else{
                    apiRequestToSendOTP(whichApiCall, ""+bic);
                }
            }
            
        }
    }
    
    private String getFormatedTid(String tid) {
//		Integer currentTime = (int)new Date().getTime();
		//long txnIDNew = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
		//String transactionId = ""+ Long.toString(txnIDNew);
		
		Calendar calender = Calendar.getInstance();
		final Integer julianYear = calender.get(Calendar.YEAR);
		final Integer julianDay = calender.get(Calendar.DAY_OF_YEAR);
		final Integer julianHrD = calender.get(Calendar.HOUR_OF_DAY);
		final Integer julianMnt = calender.get(Calendar.MINUTE);
		final Integer julianSec = calender.get(Calendar.SECOND);
		
		String yr = julianYear.toString();
		String days = julianDay.toString();
		String hrs = julianHrD.toString();
		String mnts = julianMnt.toString();
		String sec = julianSec.toString();
		if(days != null && !days.isEmpty()){
			if(days.length()<=3){
				if(days.length()==1)
					days = "00"+days;
				if(days.length()==2)
					days = "0"+days;
			}
		}
		if(hrs != null && !hrs.isEmpty()){
			if(hrs.length()!=2){
				if(hrs.length()==1)
					hrs = "0"+hrs;
			}
		}
		if(mnts != null && !mnts.isEmpty()){
			if(mnts.length()!=2){
				if(mnts.length()==1)
					mnts = "0"+mnts;
			}
		}
		if(sec != null && !sec.isEmpty()){
			if(sec.length()!=2){
				if(sec.length()==1)
					sec = "0"+sec;
			}
		}
		
		//String formatedNumber = yr.charAt(3)+"_"+julianDay+"_"+julianHrD+"_"+julianMnt+"_"+julianSec;
		String formatedNumber = yr.charAt(3)+""+days+""+hrs+""+mnts+""+sec;
		String formatedId = Constants.SELLER_IDENTIFIER+tid+formatedNumber;
//		System.out.println("formatedNumber: "+formatedNumber);
//		System.out.println("formatedId: "+formatedId);
		return formatedId;
	}
 	
    private void callOffLineTransProcess(String data){
    	
		System.out.println("offline Success");
		Map<String, String> parms = new LinkedHashMap<>();
		
		String tid = merchant.getTid();
//		String txnId = getRefCode(tid);
		String txnId = getFormatedTid(tid);
		parms.put("transaction_id", txnId);
		parms.put("amount", amount);
		parms.put("m_app_id", merchant.getApp_id());
		parms.put("m_teller_code", selectedTellerCode);
		String cId = data.substring(0,8);
		parms.put("c_app_id", cId);
		
		String bic = data.substring(8,data.length());
		parms.put("bic", bic);
		parms.put("tid", tid);
		parms.put("teller_code", selectedTellerCode);
		jlblProgressText.setText("Initiating transaction please wait...");
		
		String res = callOffLineAPI(Constants.API_OFFLINE_TRANSACTION,parms,2); // 2 is POST request
		System.out.println("res: "+res);
		try {
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(res);
			String isSuccess = json.get("success").toString();
			if(isSuccess.equalsIgnoreCase("true")){
				String track_id = json.get("track_id").toString();
                mainForm.closeDialog();
                genBill(track_id, TRANSACTION_RESULT_SUCCESS,"");
			} else if(isSuccess.equalsIgnoreCase("false")){
				String mesg = json.get("message").toString()+"";
				if(mesg != null && !mesg.isEmpty()){
					jlblProgressText.setForeground(new java.awt.Color(255, 0, 0));
					jlblProgressText.setText(mesg);
					jlblProgressText.setBackground(Color.RED);
		            pbTransactionProgress.setIndeterminate(false);
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
    
    private String callOffLineAPI(String url, Map<String, String> params, int reqMethod){

        int responseCode = 500;
        String responseMessage = null;
        String data = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// prepare parameters
			String trans_id = params.get("transaction_id").toString();
	        String amnt =  params.get("amount").toString();
	        String m_app_id = params.get("m_app_id").toString();
	        String m_teller_code = params.get("m_teller_code").toString();
	        String c_app_id =  params.get("c_app_id").toString();
	        String bic = params.get("bic").toString();
	        String tid = params.get("tid").toString();
        	data = "data={"+"\"transaction_id\""+":\""+trans_id+"\","+"\"amount\""+":\""+amnt+"\","
        			+"\"mid\""+":\""+m_app_id+"\","+"\"m_teller_code\""+":\""+m_teller_code+"\","
        			+"\"c_app_id\""+":\""+c_app_id+"\","+"\"bic\""+":\""+bic+"\","
        			+"\"tid\""+":\""+tid+"\"}";
        	System.out.println("off line data: "+data);

			// add request header
			if (reqMethod == POST) {
				con.setRequestMethod("POST");
			} else if (reqMethod == GET) {
				con.setRequestMethod("GET");
			}
			con.setRequestProperty("Authorization", Constants.TOKEN);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();

			responseCode = con.getResponseCode();
			StringBuilder response = new StringBuilder();
			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} else {
				response.append(con.getResponseMessage());
			}
			responseMessage = response.toString();
		} catch (Exception e) {
        }
        return responseMessage;
	}
    
    
    private void callOnLineTransProcess(String api){
    	
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String tid = merchant.getTid();
       String refCode = getRefCode(tid);
       params.add(new BasicNameValuePair("amount", amount));
        params.add(new BasicNameValuePair("refcode", refCode));
        params.add(new BasicNameValuePair("mid", merchant.getApp_id()));
        params.add(new BasicNameValuePair("tid", tid));
        
        params.add(new BasicNameValuePair("teller_code", selectedTellerCode));
        params.add(new BasicNameValuePair("bic", "W596"));
//        System.out.println("params: "+params);
        final String track_id = refCode;
        this.mainForm.track_Ref_ID = track_id;
        System.out.println("this.mainForm.track_Ref_ID: "+track_id);
        try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), api,NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);
        jlblProgressText.setText("transaction has been processing please wait...");
        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
            	jlblProgressText.setText("Initiating transaction please wait...");
                JsonParser jsonParser = new JsonParser((String) obj);
                if (jsonParser.isHavingSuccess()){
                	try {
            			JSONParser parser = new JSONParser();
            			JSONObject json = (JSONObject) parser.parse(obj.toString());
            			String isSuccess = json.get("success").toString();
            			if(isSuccess.equalsIgnoreCase("true")){
            				String track_id = json.get("track_id").toString();
                            mainForm.closeDialog();
                            call_TransactionMapp(track_id);
                            genBill(track_id, TRANSACTION_RESULT_SUCCESS,"");
                            stopRecorder();
                            mainForm.track_Ref_ID = null;
            			} else if(isSuccess.equalsIgnoreCase("false")){
            				String mesg = json.get("message").toString()+"";
//            				System.out.println("else: "+mesg);
            				if(mesg != null && !mesg.isEmpty()){
            					jlblProgressText.setForeground(new java.awt.Color(255, 0, 0));
            					jlblProgressText.setText(mesg);
            		            pbTransactionProgress.setIndeterminate(false);
            				}
            			}
            		} catch (ParseException e) {
            			jlblProgressText.setForeground(new java.awt.Color(255, 0, 0));
            			jlblProgressText.setText("some server problem occured please try again");
            			stopRecorder();
            			hideAndSeek();
            		}
                }else{
                	stopRecorder();
                    if(track_id!= null && !track_id.isEmpty())
                	callCancelTransProcess(track_id);
        			genBill(track_id, TRANSACTION_RESULT_FAILED, jsonParser.getMessage());
//                    hideAndSeek();
                }
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
            	if (responsePhrase != null && (responsePhrase.contains(Constants.PRODUCTION_IP)
            			|| responsePhrase.contains("No route to host"))){
                	jpTransactionProgress.setVisible(false);
                	JOptionPane.showMessageDialog(mainForm, "please check your network connection");
            	}
                if (responsePhrase != null){
                	jlblProgressText.setForeground(new java.awt.Color(255, 0, 0));
                	jlblProgressText.setText(responsePhrase);
                }
                System.out.println("tck id: "+track_id);
                if(track_id!= null && !track_id.isEmpty())
                	callCancelTransProcess(track_id);
                stopRecorder();
                hideAndSeek();
            }
        });

        mNetworkAsyncTask.start();
    
    }
    

    private void callCancelTransProcess(String transId){
    	
        List<NameValuePair> params = new ArrayList<NameValuePair>();
       params.add(new BasicNameValuePair("transaction_id", transId));
        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), Constants.API_TRANSACTION_CANCEL,NetworkAsyncTask.POST, params,
                NetworkAsyncTask.PROGRESS_NULL, null);
        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                JsonParser jsonParser = new JsonParser((String) obj);
                if (jsonParser.isHavingSuccess()){
       				JOptionPane.showMessageDialog(new Seller(), "Transaction hasbeen canceled");
//           			hideAndSeek();
                }
            }
            @Override
            public void onProcessProgress(int value) {
            }
            @Override
            public void onErrorResponse(String responsePhrase) {
            }
        });
        mNetworkAsyncTask.start();
    }
    
    public boolean call_TransactionMapp(String transId){
    	try {
			
    	/*System.out.println("orderId: "+this.mainForm.orderID);
    	System.out.println("amount: "+this.mainForm.amount);
    	System.out.println("storeNum: "+this.mainForm.storeNumber);
    	System.out.println("TermId: "+this.mainForm.terminalNumber);
    	System.out.println("phoneNum: "+this.mainForm.phone);*/
    	String url = Constants.API_TRANS__MAPPER_ID_URL;
    	String responseMessage = null;
        int responseCode = 500;
        String data = "";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        
        String locCode = this.mainForm.storeNumber;//params.get("location_code").toString();
        String compNm = this.mainForm.terminalNumber;// params.get("comp_name").toString();
        String ordId = this.mainForm.orderID;//params.get("order_id").toString();
        String phone = this.mainForm.phone;//params.get("datetime").toString();
        String trnsId = transId;
        
    	data = "data={"+"\"transaction_id\""+":\""+trnsId+"\","+"\"location_code\""+":\""+locCode+"\","
    			+"\"comp_name\""+":\""+compNm+"\","+"\"order_id\""+":\""+ordId+"\","+"\"phone\""+":\""+phone+"\"}";
    	
        con.setRequestMethod("POST");
        //con.setRequestProperty("Authorization", "Token 23116c8ea17c3dfdf65d1e191dcd495e33d82ed8ad759d5c71c65555ed812dd9");
        con.setRequestProperty("Authorization", Constants.TOKEN);
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();

        responseCode = con.getResponseCode();

        StringBuilder response = new StringBuilder();
        if(responseCode == 200){
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        }else{
            response.append(con.getResponseMessage());
        }
        responseMessage = response.toString();
        System.out.println("res: "+responseMessage);
        if(responseMessage.contains("true")){
        	return true;
        } else {
        	call_TransactionMapp(transId);
        }
		} catch (Exception e) {
		}
        return false;
	}
 
    private String getRefCode(String tid) {
    		Properties props = new Properties();
    		String formatedTid = "";
    		try {
        		/*ClassLoader cl = getClass().getClassLoader();		
        		InputStream in = cl.getResourceAsStream(Constants.TID_FILE_NAME);
        		*/
        		FileInputStream in;
    			in = new FileInputStream(Constants.TID_FILE_NAME);
        		
        		props.load(in);
        		in.close();
        		formatedTid = props.getProperty(tid)+"";
        		if(formatedTid != null && !formatedTid.isEmpty())
        			return formatedTid;
			} catch (Exception e) {
				e.printStackTrace();
			}
    		return formatedTid;
	}
    
    private void saveTID_in_File(String tid, String formatedTid){
		Properties prop = new Properties();
		OutputStream output = null;
		try {
			//String formatedTid = getFormatedTid(tid);
			File f = new File(Constants.TID_FILE_NAME);
			if(!f.canWrite())
				f.setWritable(true);
			output = new FileOutputStream(f);
			// set the properties value
			prop.setProperty(tid, formatedTid);

			// save properties to project root folder
			prop.store(output, null);
			//f.setReadOnly();
			
		} catch (IOException io) {
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private class OnDataReceived implements SoundRecorder.OnDataFoundListener{

        @Override
        public void onDataFound(String data, int i, boolean bln, short s) {
    		try {
				if (data != null && !data.isEmpty()) {
					callOffLineTransProcess(data);
				} 
			} catch (Exception e) {
				System.out.println("off exception");
				stopRecorder();
				stopPlayer();
				triggerToMainPanel();
			}
		}

        @Override
        public void onDataFound(long l, int i, short s) {
        	System.out.println();
        }
        
    }
	
	private class OnSoundDataReceived implements SoundRecorder.OnDataFoundListener{

        @Override
        public void onDataFound(String data, int i, boolean bln, short s) {
            //System.out.println("Received data : " + data);
            /*if (mode == TXN_INITIALISATION) {
                startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, jlblAmount.getText());
            }*/
            try{
                // call the api to send customer info and send the amount to customer
                receivedString = data;
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showLoadingDialog(getResources().getString(R.string.process_msg_three));
                    }
                });*/
                if (whichApiCall == OTP_API_CALL){
                    //audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                    /*profileSet = true;
                    mSoundPlayer.setDeviceVolume(volLevel);*/
                    customerIdentificationFlag = data.charAt(11);
                    bic = data.substring(data.length()-4,data.length());
                    char bicIdentifier = bic.charAt(0);
                    modeOfPayment = bicIdentifier;
                    //System.out.println("Mode of payment : " + modeOfPayment);

                    //System.out.println("customerIdentificationFlag is " + customerIdentificationFlag);

                    final String totalAmount = customerIdentificationFlag + amount;
                    //System.out.println("totalAmount is " + totalAmount);
                    if(modeOfPayment == 'W'){
                        int flow = Integer.parseInt(data.charAt(10)+"");
                        if(flow == TXN_ONE_WAY){
                            //System.out.println("Mode One way");
                            //apiRequestToSendOTP(whichApiCall, ""+bic);
                            jlblProgressText.setText("Initiating Transaction please wait..");
                        }else{
                            //System.out.println("Mode two way");
                            jlblProgressText.setText("Sending Invoce to Customer");
                            startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, totalAmount);
                        }

                    }else if(modeOfPayment == 'C'){
                        if(!bic.equalsIgnoreCase("C420")){
                            //System.out.println("!bic.equalsIgnoreCase(\"C420\")");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    jlblProgressText.setText("Sending Invoce to Customer");
                                    startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, totalAmount);
                                }
                            }).start();
                            
                        }else{
                            //System.out.println("!bic.equalsIgnoreCase(\"C420\")" + !bic.equalsIgnoreCase("C420"));
                            
                        }
                        
                    }
                    //jlblProgressText.setText("Sending Invoce to Customer");
                    //startPlayer(SoundRecorder.AMOUNT_RECORD_MODE, totalAmount);
                }
                if(whichApiCall == TXN_API_CALL){
                    jlblProgressText.setText("Authenticating Transaction please wait..");
                }

                //System.out.println("Alway print");
                //if(!bic.equalsIgnoreCase("C420"))
                apiRequestToSendOTP(whichApiCall, data);
            }catch(Exception e){
                stopRecorder();
                stopPlayer();
                triggerToMainPanel();
            }
        }

        @Override
        public void onDataFound(long l, int i, short s) {
        }
        
    }
    

    private void sendOnlineDetails(List<NameValuePair> paramsValueReq,
                                                       String apiConstantsStr, final String apiFlag) {
        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), apiConstantsStr,NetworkAsyncTask.POST, paramsValueReq,
                NetworkAsyncTask.PROGRESS_NULL, null);

        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                JsonParser jsonParser = new JsonParser((String) obj);

                if (jsonParser.isHavingSuccess()) {
                    /*if (apiFlag.equalsIgnoreCase("TxnFlag"))
                    {
                        // Response Success for Getting Txn ID
                        strTxnIdOnlineUpdated = jsonParser.getOnlineCardTransactionID();
                        mainForm.closeDialog();

                    } else {
                        // Response for Getting Txn Status Success
                        // {:success => true, :transaction_status => "Success"}

                        String txnStatusObj = jsonParser.getOnlineCardTransactionStatus();//onjJsonRes.getString("transaction_status");
                        //L.e("Status Check TxnID Data --> ", "Success : " + amount);
                        //L.e("Txn Status Check Success ", strTxnIdOnlineUpdated +" -> " + txnStatusObj);
                        timerTask.cancel();

                        genBill(strTxnIdOnlineUpdated, TRANSACTION_RESULT_SUCCESS,""); // responseOnlineTxnStr.toString());
                        
                    }*/

                } else {

                    if (apiFlag.equalsIgnoreCase("TxnFlag")) {

                    // Response for Getting Txn ID  --> (Success --> False)

                        JOptionPane.showMessageDialog(mainForm,
                                "Currently  debit/credit Card Transaction are not possible.",
                                "Transaction warning",
                                JOptionPane.WARNING_MESSAGE);
//                        try {
//                            String message = onjJsonRes.getString("message");
//                            //L.e("Txn Id False ", " -> " + message);
//                            // mSoundPlayer.sendString("01"); // 01 = Merchant or Customer Not Found
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    } else {

                        // Response for Getting Txn Status (Success --> False)

                        /*:success => false, :transaction_status => "Pending",:message => "Pending transaction."

                        {:success => false, :transaction_status => "Failed",:message => "Transaction failed."} */


                        String txnStatusObj = jsonParser.getOnlineCardTransactionStatus();
                        if(txnStatusObj.equalsIgnoreCase("Pending"))
                        {
                            //L.e("Txn Status Check Pending ", txnStatusObj +" -> " + message);
                            startTimerTxnStatusCheck();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    jlblProgressText.setText("Processing Transaction...");
                                }
                            }).start();
                        }else if(txnStatusObj.equalsIgnoreCase("Failed"))
                        {
                            //L.e("Txn Status Check Failed ", txnStatusObj +" -> " + message);
                            timerTask.cancel();
                            genBill(strTxnIdOnlineUpdated, TRANSACTION_RESULT_FAILED,
                                    "Transaction failed.");
                        }
                    }
                    
                }
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
//                showAlertDialog(ProcessActivity.this,
//                        getResources().getString(R.string.str_version_check_failed_title),
//                        responsePhrase, null, null);

                String message = "";
                
                if(!apiFlag.equalsIgnoreCase("TxnFlag")){
                    
                    if (responsePhrase != null) message = responsePhrase;
                    if(track_ID == null || track_ID.isEmpty()) track_ID = "";
                    stopPlayer();
                    stopRecorder();
                    timerTask.cancel();
                    genBill(track_ID, TRANSACTION_RESULT_FAILED, message);
                    
                }else{
                    mainForm.closeDialog();
                    JOptionPane.showMessageDialog(mainForm,
                                "Currently debit/credit Card Transaction are not possible.",
                                "Transaction warning",
                                JOptionPane.WARNING_MESSAGE);
                }
                
            }
        });

        mNetworkAsyncTask.start();
    }
    
    
    
    
    private void sendOnlineDetailsAndCheckTxnToBackend(List<NameValuePair> paramsValueReq,
                                                       String apiConstantsStr, final String apiFlag) {
        // String bicOnline,String amountOnline, String mAppIdOnline, String cAppIdOnline, String tellerCodeOnline)

        //L.e(TAG + "Req @ OnlineTxn --> ", paramsValueReq.toString() + "Flag : " + apiFlag + " APi : "+ apiConstantsStr );


        //AlertDialog dialog = null;
//        if(apiFlag.equalsIgnoreCase("TxnFlag"))
//        {
//            dialog =  initProgressDialog("Please wait...");
//
//        } else {
//
//            dialog = null;
//        }

        if(apiFlag.equalsIgnoreCase("TxnFlag")){
            mainForm.startDialog("Please  wait preparing for transaction...");
        }

        mNetworkAsyncTask = new NetworkAsyncTask(createSetting(), apiConstantsStr,NetworkAsyncTask.POST, paramsValueReq,
                NetworkAsyncTask.PROGRESS_NULL, null);
                // ,null);

        mNetworkAsyncTask.setOnBackgroundPrcessListener(new NetworkAsyncTask.OnBackgroundPrcessListener() {
            @Override
            public void onProcessComplet(Object obj) {
                //L.e(TAG + "--> ", (String) obj);

//                String responseOnlineTxnStr = (String) obj;
//                String txnIdOnline = null;
                JsonParser jsonParser = new JsonParser((String) obj);

                if (jsonParser.isHavingSuccess()) {
                    if (apiFlag.equalsIgnoreCase("TxnFlag"))
                    {
                        // Response Success for Getting Txn ID
                        strTxnIdOnlineUpdated = jsonParser.getOnlineCardTransactionID();
                        mainForm.closeDialog();

                    } else {
                        // Response for Getting Txn Status Success
                        // {:success => true, :transaction_status => "Success"}

                        String txnStatusObj = jsonParser.getOnlineCardTransactionStatus();//onjJsonRes.getString("transaction_status");
                        //L.e("Status Check TxnID Data --> ", "Success : " + amount);
                        //L.e("Txn Status Check Success ", strTxnIdOnlineUpdated +" -> " + txnStatusObj);
                        timerTask.cancel();

                        genBill(strTxnIdOnlineUpdated, TRANSACTION_RESULT_SUCCESS,""); // responseOnlineTxnStr.toString());
                        
                    }

                } else {

                    if (apiFlag.equalsIgnoreCase("TxnFlag")) {

                    // Response for Getting Txn ID  --> (Success --> False)

                        JOptionPane.showMessageDialog(mainForm,
                                "Currently  debit/credit Card Transaction are not possible.",
                                "Transaction warning",
                                JOptionPane.WARNING_MESSAGE);
//                        try {
//                            String message = onjJsonRes.getString("message");
//                            //L.e("Txn Id False ", " -> " + message);
//                            // mSoundPlayer.sendString("01"); // 01 = Merchant or Customer Not Found
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    } else {

                        // Response for Getting Txn Status (Success --> False)

                        /*:success => false, :transaction_status => "Pending",:message => "Pending transaction."

                        {:success => false, :transaction_status => "Failed",:message => "Transaction failed."} */


                        String txnStatusObj = jsonParser.getOnlineCardTransactionStatus();
                        if(txnStatusObj.equalsIgnoreCase("Pending"))
                        {
                            //L.e("Txn Status Check Pending ", txnStatusObj +" -> " + message);
                            startTimerTxnStatusCheck();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    jlblProgressText.setText("Processing Transaction...");
                                }
                            }).start();
                        }else if(txnStatusObj.equalsIgnoreCase("Failed"))
                        {
                            //L.e("Txn Status Check Failed ", txnStatusObj +" -> " + message);
                            timerTask.cancel();
                            genBill(strTxnIdOnlineUpdated, TRANSACTION_RESULT_FAILED,
                                    "Transaction failed.");
                        }
                    }
                    
                }
            }

            @Override
            public void onProcessProgress(int value) {

            }

            @Override
            public void onErrorResponse(String responsePhrase) {
//                showAlertDialog(ProcessActivity.this,
//                        getResources().getString(R.string.str_version_check_failed_title),
//                        responsePhrase, null, null);

                String message = "";
                
                if(!apiFlag.equalsIgnoreCase("TxnFlag")){
                    
                    if (responsePhrase != null) message = responsePhrase;
                    if(track_ID == null || track_ID.isEmpty()) track_ID = "";
                    stopPlayer();
                    stopRecorder();
                    timerTask.cancel();
                    genBill(track_ID, TRANSACTION_RESULT_FAILED, message);
                    
                }else{
                    mainForm.closeDialog();
                    JOptionPane.showMessageDialog(mainForm,
                                "Currently debit/credit Card Transaction are not possible.",
                                "Transaction warning",
                                JOptionPane.WARNING_MESSAGE);
                }
                
            }
        });

        mNetworkAsyncTask.start();
    }
    
    private void sendInvoice(){
        
        jlblProgressText.setText("Sending Invoce to Customer For Online Transaction");
        //System.out.println("strTxnIdOnlineUpdated + amount : " + strTxnIdOnlineUpdated + amount);
        try{
            startPlayer(SoundRecorder.STRING_RECORD_MODE, (strTxnIdOnlineUpdated + amount));
        }catch(Exception e){
            
        }
        
//        SoundPlayer invoicePlayer = new SoundPlayer(speakerDriver);
//        invoicePlayer.sendString(strTxnIdOnlineUpdated + amount);
    }
    
    public void startTimerTxnStatusCheck() {

        /*// run
        timer = new Timer();

        initializeTimerTask();
        //schedule the timer, after the first 10000ms the TimerTask will run every 10000ms

        timer.schedule(timerTask, 10000, 5000); //*/

        long start = System.currentTimeMillis();
        long end = start + 5*60*1000;

        if (System.currentTimeMillis() < end)
        {
            timer = new Timer();

            initializeTimerTask();
            //schedule the timer, after the first 10000ms the TimerTask will run every 2000ms

            timer.schedule(timerTask, 10000, 2000); //

        } else {
            timerTask.cancel();
            genBill(track_ID, TRANSACTION_RESULT_FAILED, "Transaction Time Out");
            //retryOnlineCardBtn.setVisibility(View.VISIBLE);

        }
    }



    public void initializeTimerTask() {

        timerTask = new TimerTask() {

            public void run() {

                new Thread(new Runnable() {
                    public void run() {

                        // Log.e("Time Diff ---->  -> ", " --->  " + System.currentTimeMillis());
                        timerTask.cancel();

                        ArrayList<NameValuePair> paramsValuetxnIdStatus = new ArrayList<NameValuePair>();
                        paramsValuetxnIdStatus.add(new BasicNameValuePair("transaction_id", strTxnIdOnlineUpdated));

/*                        sendOnlineDetailsAndCheckTxnToBackend(paramsValuetxnIdStatus,
                                Constants.API_ONLINE_TXN_ID_STATUS_CHECK, "txnStatusCheck");*/

                    }
                }).start();
            }
        };
    }

    private void triggerToResultPanel(){
        ResultPanel rp = new ResultPanel(mainForm);
        rp.setResultValue(bill);
        jpBasePanel.removeAll();
        jpBasePanel.add(rp);
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    
    private void triggerToMainPanel(){
        jpBasePanel.removeAll();
        jpBasePanel.add(new AmountPanel(mainForm));
        jpBasePanel.repaint();
        jpBasePanel.revalidate();
    }
    private void stopRecorder(){
        //System.out.println("Recorder Stoped");
        if(mSoundRecorder != null){
            //if(mSoundRecorder.isRecordingOn())
                mSoundRecorder.stopRecording();
                //mSoundRecorder = null;
        }  
    }
    
    private Settings createSetting() {
        return mainForm.mSettings;
    }
    
    private void stopPlayer(){
        if(mSoundPlayer != null){
            //mSoundPlayer = null;
        }
    }
    
    
    /*public void setOnDialoUpdateListener(OnDialoUpdateListener mOnDialoUpdateListener) {
        this.mOnDialoUpdateListener = mOnDialoUpdateListener;
    }
    
    public interface OnDialoUpdateListener{
        public void setText(String message);
        
        public void closeDialog();
    }
    
    private void startDialog(String message){
        
        mainForm.setEnabled(false);
        mDialogFrame = new DialogFrame(this);
        mDialogFrame.invokeDialog();
        if(this.mOnDialoUpdateListener != null){
            mOnDialoUpdateListener.setText(message);
        }
        //mDialogFrame.setMessage(message);
        
    }
    
    private void closeDialog(){
        mainForm.setEnabled(true);
        if(mOnDialoUpdateListener != null){
            mOnDialoUpdateListener.closeDialog();//setText("Sending Data to Customer");
        }
    }*/
    
    private DialogFrame mDialogFrame;
    //private OnDialoUpdateListener mOnDialoUpdateListener;
    private SoundRecorder mSoundRecorder;
    private SoundPlayer mSoundPlayer;
    private boolean isTxnFailed = false;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton jbCancel;
    private javax.swing.JButton jbProceed;
    private javax.swing.JLabel jlblAmount;
    private javax.swing.JLabel jlblProcessTxn;
    private javax.swing.JLabel jlblProgressText;
    private javax.swing.JPanel jpTransaction;
    private javax.swing.JPanel jpTransactionProgress;
    private javax.swing.JProgressBar pbTransactionProgress;
    // End of variables declaration//GEN-END:variables
}
