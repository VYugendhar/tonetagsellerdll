/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tonetag.desktop.seller.tasks;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
//import com.tonetag.merchant.util.L;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.tonetag.desktop.seller.bean.AccountDetails;
import com.tonetag.desktop.seller.bean.Merchant;
import com.tonetag.desktop.seller.bean.Settings;
import com.tonetag.desktop.seller.bean.Teller;
import com.tonetag.desktop.seller.bean.TransactionLog;
import com.tonetag.desktop.seller.implementations.FileLogger;
import com.tonetag.desktop.seller.interfaces.Logger;

/**
 * Class to parse json string
 */
public class JsonParser {

    private static final String  TAG = JsonParser.class.getSimpleName();

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_MERCHANT_DETAILS = "merchant_details";
    private static final String TAG_NAME = "name";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PHONE = "phone";
    private static final String TAG_APP_ID = "app_id";
    private static final String TAG_BIC = "bic";
    private static final String TAG_WALLET_SPEC = "walletspec";
    private static final String TAG_KYC_STATUS = "kyc_status";
    private static final String TAG_ERROR = "error";
    private static final String TAG_BLOCKED_CALLER = "caller";
    private static final String TAG_AUTH = "authentication";
    private static final String TAG_AUTH_TYPE = "auth_type";
    private static final String TAG_AUTH_KEY = "auth_key";
    private static final String TAG_MERCHANT_ACCOUNT_DETAILS = "account_details";
    private static final String TAG_MERCHANT_TELLER_DETAILS = "teller_details";
    private static final String TAG_TELLER_NAME = "name";
    private static final String TAG_TELLER_CODE = "teller_code";
    private static final String TAG_TELLER_PHONE = "phone";
    private static final String TAG_TELLER_ACTION = "teller_action";
    private static final String TAG_MERCHANT_ACCOUNT_TYPE = "account_type";
    private static final String TAG_MERCHANT_ACCOUNT_NAME = "account_name";
    private static final String TAG_MERCHANT_ACCOUNT_NUMBER = "account_number";
    private static final String TAG_MERCHANT_ACCOUNT_IFSC_CODE = "ifsc_code";
    private static final String TAG_MERCHANT_ACCOUNT_STATUS = "account_status";
    private static final String TAG_MERCHANT_ACCOUNT_RE_ACTIVATE = "re_activate";
    private static final String TAG_ONLINE_TRANSACTION_ID = "track_id";
    private static final String TAG_ONLINE_TRANSACTION_STATUS = "transaction_status";
    
    private static final String TAG_DEVICE_DETAILS = "device_details";
    private static final String TAG_TID = "tid";
    
    // Settings key names
    private static final String TAG_HOST = "host";
    private static final String TAG_PORT = "port";
    private static final String TAG_MIC_MIXER = "micMixer";
    private static final String TAG_SPEAKER_MIXER = "speakerMixer";

    private static final String TAG_TRANSACTION_TRACK_ID = "track_id";
    private static final String TAG_TRANSACTION_ID = "transaction_id";

    public static final String TAG_KYC_UPLOADED = "kyc_uploaded";

    private static final String TAG_PROVIDERS = "wallets";

    
    private JSONObject mJsonObject;
    private  String response;
    private File responseFile;
    private Logger mLogger;

    /**
     * Default constructor to create plain instance
     */
    public JsonParser(){

    }

    /**
     * Override constructor which initialises object for response string.
     *
     * @param response json response string.
     */
    public JsonParser(String response){
        this.response = response;
        try {
            mLogger = new FileLogger();
        } catch (IOException ex) {
            System.out.println("FileLog Creation Exception : " + ex.getLocalizedMessage());
        }
        Object obj;
        try {
            obj = new JSONParser().parse(response);
            mJsonObject = (JSONObject)obj;
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }
    }
    
    /**
     * Override constructor which initialises object for response string.
     *
     * @param response json response string.
     */
    public JsonParser(File responseFile){
        this.responseFile = responseFile;
        Object obj;
        try {
            obj = new JSONParser().parse(new FileReader(responseFile));
            mJsonObject = (JSONObject)obj;
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }
    }

    /**
     * Gives true or false based on value of success key
     *
     * @return boolean represents success tag from the response json
     */
    public boolean isHavingSuccess() {
        boolean success = false;
        try {
            success = (Boolean)mJsonObject.get(TAG_SUCCESS);
        } catch (Exception je){
            success = false;
        }
        return success;
    }

    public String getBlockedNumber(){
        String number = "";
        try {
            number += mJsonObject.get(TAG_BLOCKED_CALLER).toString();
        } catch (Exception je){
            number = null;
        }
        return number;
    }

    /**
     * method to get error tag value in json response
     *
     * @return boolean represents error tag from the response json
     */
    public boolean isHavingError(){
        boolean error = false;
        Object obj;
        try {
            obj = new JSONParser().parse(response);
            mJsonObject = (JSONObject)obj;
            error = (Boolean)mJsonObject.get(TAG_ERROR);
        } catch (Exception e) {
            error = false;
            if(mLogger != null)
                mLogger.writeEntry(e);
        }

        return error;
    }

    public JSONObject createRegRequest(HashMap<String, Object> keyValuePair){
        JSONObject requestObj = null;
        if (keyValuePair != null) {
            requestObj = new JSONObject(keyValuePair);
        }
        return requestObj;
    }

    public Merchant parseToMerchant(){
        Merchant merchant = new Merchant();
        List<AccountDetails> accounts = new ArrayList<AccountDetails>();
        List<Teller> tellers = new ArrayList<Teller>();
        try {
            JSONObject dataObj = (JSONObject)mJsonObject.get(TAG_MERCHANT_DETAILS);
            merchant.setName((String)dataObj.get(TAG_NAME));
            merchant.setEmail((String)dataObj.get(TAG_EMAIL));
            merchant.setNumber((String)dataObj.get(TAG_PHONE));
            merchant.setApp_id((String)dataObj.get(TAG_APP_ID));
            
        	JSONObject innerJsonObject = (JSONObject) mJsonObject.get(TAG_DEVICE_DETAILS);
            merchant.setTid((String)innerJsonObject.get(TAG_TID).toString());
            
            merchant.setKyc_status((String)dataObj.get(TAG_KYC_STATUS));
            merchant.setKyc_uploaded((Boolean)dataObj.get(TAG_KYC_UPLOADED));
            JSONArray accountArray = (JSONArray)dataObj.get(TAG_MERCHANT_ACCOUNT_DETAILS);
            for(int i = 0; i < accountArray.size(); i++){
                AccountDetails accountDetails = new AccountDetails();
                JSONObject account = (JSONObject)accountArray.get(i);
                /*accountDetails.setAccType(account.getString(TAG_MERCHANT_ACCOUNT_TYPE));
                accountDetails.setName(account.getString(TAG_MERCHANT_ACCOUNT_NAME));
                accountDetails.setAccNo(account.getString(TAG_MERCHANT_ACCOUNT_NUMBER));
                accountDetails.setIfscCode(account.getString(TAG_MERCHANT_ACCOUNT_IFSC_CODE));*/
                accountDetails.setStatus((Boolean)account.get(TAG_MERCHANT_ACCOUNT_STATUS));
                accounts.add(accountDetails);
            }
            merchant.setAccountDetails(accounts);
            JSONArray tellerArray = (JSONArray)mJsonObject.get(TAG_MERCHANT_TELLER_DETAILS);
            for(int i = 0; i < tellerArray.size(); i++){
                Teller teller = new Teller();
                JSONObject tellerObject = (JSONObject)tellerArray.get(i);
                teller.setName((String)tellerObject.get(TAG_TELLER_NAME));
                teller.setPhone((String)tellerObject.get(TAG_TELLER_PHONE));
                teller.setTeller_code((String)tellerObject.get(TAG_TELLER_CODE));
                tellers.add(teller);
            }
            merchant.setTellers(tellers);

        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
            //Log.e(TAG, e.getMessage());
        }

        return merchant;
    }
    
    public Settings parseToSetting(){
        Settings settings = new Settings();
        
        try{
//            settings.setHost((String) mJsonObject.get(TAG_HOST));
//            settings.setPort(Integer.parseInt((String) mJsonObject.get(TAG_PORT)));
//            settings.setMicMixer((String) mJsonObject.get(TAG_MIC_MIXER));
            settings.setSpeakerMixer((String) mJsonObject.get(TAG_SPEAKER_MIXER));
            
        }catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
            //Log.e(TAG, e.getMessage());
        }
        
        return settings;
    }

    public boolean isMessageOk(){
        boolean message = false;
        Object obj;
        try {
            obj = (Object) new JSONParser().parse(response);
            mJsonObject = (JSONObject) obj;
            if(mJsonObject.containsKey(TAG_MESSAGE)){
                String msg = (String)mJsonObject.get(TAG_MESSAGE);
                //comment(TAG, "massage -------- " + msg);
                if(msg.equalsIgnoreCase("OK")){
                    message=true;
                }
            }
        } catch (Exception je){
            message = false;
            if(mLogger != null)
                mLogger.writeEntry(je);
        }
        return message;
    }

    /**
     * method extract track id from the json response
     *
     * @return String track id
     */
    public String getTrackId(){
        String trackId = "";
        try {
            trackId = (String)mJsonObject.get(TAG_TRANSACTION_TRACK_ID);
        } catch (Exception je){
            if(mLogger != null)
                mLogger.writeEntry(je);
            //comment(TAG, je.getMessage());
        }
        return trackId;
    }

    public String getTID(){
    	String tid = "";
        try {
        	JSONObject innerJsonObject = (JSONObject) mJsonObject.get(TAG_DEVICE_DETAILS);
        	tid = innerJsonObject.get(TAG_TID).toString();
        } catch (Exception je){
            if(mLogger != null)
                mLogger.writeEntry(je);
        }
    	return tid;
    }
    
    
    /**
     * get value by tag kyc_status
     *
     * @return String status of Pending, Fail and Success
     */
    public String getTagKycStatus(){
        String status = null;
        
        try {
            status = (String)mJsonObject.get(TAG_KYC_STATUS);
            //comment(TAG, status);
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
            //comment(TAG, e.getMessage());
        }
        return  status;
    }

    /**
     * get value of uploading kyc files
     *
     * @return status of kyc file uploading status
     */
    public boolean isKycUploaded(){
        boolean isUploaded = false;

        try {
            isUploaded = (Boolean)mJsonObject.get(TAG_KYC_UPLOADED);
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }

        return isUploaded;
    }

    /**
     * method extract transaction id from the json response
     *
     * @return String transaction id
     */
    public String transactionID(){
        String transactionId = "";
        try {
            transactionId = (String)mJsonObject.get(TAG_TRANSACTION_ID);
            //comment(TAG, "transaction id -------- " + transactionId);
        } catch (Exception je){
            if(mLogger != null)
                mLogger.writeEntry(je);
        }
        return transactionId;
    }

    public String getMessage(){
        String message = "";
        try {
            message = (String)mJsonObject.get(TAG_MESSAGE);
            //comment(TAG, "error message -------- " + message);
        } catch (Exception je){
            if(mLogger != null)
                mLogger.writeEntry(je);
        }
        return message;
    }

    public String getKYCFailMessage(){
        String message = null;

        try {
            message = (String)mJsonObject.get("message");
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }

        return message;
    }

    public Map<String, String> getTxnValues(){
        Map<String, String> map = new HashMap<String, String>();

        try{
            map.put("bic", (String)mJsonObject.get(TAG_BIC));
            map.put("walletspec", (String)mJsonObject.get(TAG_WALLET_SPEC));
        }catch (Exception jex){
            map = null;
            if(mLogger != null)
                mLogger.writeEntry(jex);
        }

        return map;
    }

    /**
     * helper class to manage merchant verification Status
     */
    public class MerchantVerification{
        public boolean reActivate = false;
        public String message = null;


        /**
         * Constructor to create instance of an Merchant Verification object
         *
         * @param reActivate reactivation boolean flag
         * @param message String message of response
         */
        MerchantVerification(boolean reActivate, String message){
            this.reActivate = reActivate;
            this.message = message;
        }
    }

    /**
     * if merchant verification fails then get re activation status and message
     *
     * @return {@link com.tonetag.merchant.tasks.JsonParser.MerchantVerification} object
     */
    public MerchantVerification merchantOtpVerifiactionFalse(){
        MerchantVerification merchantVerification = null;

        try {
            Boolean reActivationStatus = (Boolean)mJsonObject.get(TAG_MERCHANT_ACCOUNT_RE_ACTIVATE);
            String message = (String)mJsonObject.get(TAG_MESSAGE);
            merchantVerification = new MerchantVerification(reActivationStatus, message);

        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }

        return merchantVerification;
    }

    public String kycStatus(){
        String status = null;

        try {
            status = (String)mJsonObject.get(TAG_KYC_STATUS);
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }

        return status;
    }

    /**
     * returns the teller object from json response
     *
     * @return {@link Teller} object
     */
    public Teller getTeller(){
        Teller teller = null;

        try {
            String tellerName = (String)mJsonObject.get(TAG_TELLER_NAME);
            String tellerPhone = (String)mJsonObject.get(TAG_TELLER_PHONE);
            String tellerCode = (String)mJsonObject.get(TAG_TELLER_CODE);

            teller = new Teller(tellerName, tellerPhone, tellerCode);

        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
            //Log.e(TAG, "error while getting Teller : " + e.getMessage());
        }

        return teller;
    }

    /**
     * method returns String type action whether to add or remove
     *
     * @return String "add" or "delete"
     */
    public String getTellerAction(){
        String tellerAction = null;

        try {
            tellerAction = (String)mJsonObject.get(TAG_TELLER_ACTION);
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }

        return tellerAction;
    }
    
    
    public String getOnlineCardTransactionID(){
        
        String txnId = null;
        
        try {
            txnId = (String)mJsonObject.get(TAG_ONLINE_TRANSACTION_ID);
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }
        
        return txnId;
    }
    
    public String getOnlineCardTransactionStatus(){
        String status = null;
        
        try {
            status = (String)mJsonObject.get(TAG_ONLINE_TRANSACTION_STATUS);
        } catch (Exception e) {
            if(mLogger != null)
                mLogger.writeEntry(e);
        }
        
        return status;
    }
    
    public double getTotalAmount(){

    	double totalAmount = 0;
        try {
        	String tAmount = mJsonObject.get("total_amt").toString();
        	totalAmount = Double.parseDouble(tAmount);
        } catch(Exception e){
            if(mLogger != null)
                mLogger.writeEntry(e);
        }
    	return totalAmount;
    }

    public ArrayList<TransactionLog> getTransactionLogs(){
        ArrayList<TransactionLog> arrayListTrans = new ArrayList<TransactionLog>();
        
        try {

            JSONArray arrayTrans =(JSONArray) mJsonObject.get("transaction_logs");

            for (int i = 0; i < arrayTrans.size(); i++) {
                JSONObject objectTransAr = (JSONObject)arrayTrans.get(i);

                TransactionLog transactionModel = new TransactionLog();
                transactionModel.setTransId((String)objectTransAr.get("transaction_id"));
//                transactionModel.setTransAmount(objectTransAr.get("amount").toString());
                double amnt = Double.parseDouble(objectTransAr.get("amount")+"");
                transactionModel.setAmount(amnt);
                transactionModel.setTransCreatedOn((String)objectTransAr.get("created_on"));
                transactionModel.setStatus(getStatus(objectTransAr.get("status").toString()));
                
                arrayListTrans.add(transactionModel);
            }

        } catch(Exception e){
            if(mLogger != null)
                mLogger.writeEntry(e);
        }
        return arrayListTrans;
    }
    
    private String getStatus(String statusCode){
    	String status = "";
    	switch (statusCode) {
		case "2":
			status = "success";
			break;
		case "4":
			status = "failed ";
			break;

		default:
			break;
		}
    	return status;
    }

}

