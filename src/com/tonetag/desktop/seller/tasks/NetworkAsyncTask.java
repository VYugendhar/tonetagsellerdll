package com.tonetag.desktop.seller.tasks;

import com.tonetag.desktop.seller.bean.Settings;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * class creates Async task to perform network calls
 */
public class NetworkAsyncTask extends Thread{
    // Progress dialog related
    public static final String DEFAULT_PROGRESS_MSG = "Process Running Wait";
    public static final int PROGRESS_NULL = 0;
    public static final int PROGRESS_INDETERMINATE = 1;
    public static final int PROGRESS_DETERMINATE = 2;

    private OnBackgroundPrcessListener listener;
    public final static int GET = 1;
    public final static int POST = 2;

    private int method = 1;

    static String response = null;

    private String errorMessage;
    private int statusCode;
    private ServiceHandler sh;
    //private ServiceHandlerDominos sh;

    private String api = null;
    private List<NameValuePair> params = null;
    private int progressType;
    
    private Settings settings;

    public NetworkAsyncTask(Settings settings,String api, int method, List<NameValuePair> params, int progressType, Object progressDialog){
        this.api = api;
        this.params = params;
        this.progressType = progressType;
        this.method = method;
        this.settings = settings;
    }

    
    protected void onPreExecute() {
        
    }

    @Override
    public void run() {
        //To change body of generated methods, choose Tools | Templates.
        onPostExecute(doInBackground());
    }
    
    

    protected String doInBackground() {
        sh = new ServiceHandler();
        //sh = new ServiceHandlerDominos();
        String response = null;
        if(method == POST){
            //response = sh.makeServiceCallForDominos(settings, api, ServiceHandler.POST, params);//sh.makeServiceCall(api, ServiceHandler.POST, params);
            response = sh.makeServiceCall(api, ServiceHandler.POST, params);
        }
        if(method == GET){
            response = sh.makeServiceCall(api, ServiceHandler.GET, params);
        }

        return response;
    }
    
    protected void onPostExecute(String response) {
        
        if(response != null){
            if (listener != null)
            listener.onProcessComplet(response);
        }else{
            if (listener != null){
                listener.onErrorResponse(sh.getErrorMessage());
                //listener.onProcessComplet("{\"success\":false}");
            }
        }
    }

    /**
     *
     * @param listener
     */
    public void setOnBackgroundPrcessListener(OnBackgroundPrcessListener listener){
        this.listener = listener;
    }

    /**
     * Listener Interface to inform about background process
     */
    public interface OnBackgroundPrcessListener{
        /**
         * Override the method to get response object
         * from api call
         *
         * @param obj response of Type Object
         */
        void onProcessComplet(Object obj);

        /**
         * override the method to get the progress value from background process
         *
         * @param value integer type of progress
         */
        void onProcessProgress(int value);

        /**
         * call back when htttp response error occurs other than 200
         *
         * @param responsePhrase string http response phrase
         */
        void onErrorResponse(String responsePhrase);
    }
}
