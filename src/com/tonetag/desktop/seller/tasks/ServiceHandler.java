package com.tonetag.desktop.seller.tasks;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
//import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
//import org.json.JSONTokener;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.List;

/**
 * makes Http operations
 */
public class ServiceHandler {

    private  String response = null;
    public final static int GET = 1;
    public final static int POST = 2;

    private String errorMessage;
    private int statusCode;

    public ServiceHandler() {

    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
                                  List<NameValuePair> params) {
        //Log.e("ServiceHandler", url);
        try {
            // http client
            // set the connection timeout value to 15 seconds (15000 milliseconds)
            final HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            //System.out.println("URL : " + url);
            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                	httpPost.setHeader("Authorization", "Token 23116c8ea17c3dfdf65d1e191dcd495e33d82ed8ad759d5c71c65555ed812dd9");
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }

                httpResponse = httpClient.execute(httpPost);
                statusCode = httpResponse.getStatusLine().getStatusCode();
                errorMessage = httpResponse.getStatusLine().getReasonPhrase();
            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    //url += ":" + paramString;
                    url +="?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);

                httpResponse = httpClient.execute(httpGet);
                statusCode = httpResponse.getStatusLine().getStatusCode();
                errorMessage = httpResponse.getStatusLine().getReasonPhrase();
            }
            //System.out.println("statusCode : " + statusCode);
            
            if (statusCode == HttpURLConnection.HTTP_OK){
                httpEntity = httpResponse.getEntity();
                String tempResponse = EntityUtils.toString(httpEntity);
                //System.out.println("Response : " + tempResponse);
                try {
                    Object json = new JSONParser().parse(tempResponse);
                    if ((json instanceof JSONObject) || (json instanceof JSONArray)){
                        try{
                            boolean b = (boolean)((JSONObject)json).get("success");
                            response = tempResponse;
                        }catch(Exception e){
                            response = null;
                            errorMessage = "Bad response from server";
                        }
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                    response = null;
                    errorMessage = "Bad response from server";
                }

            }else{
                response = null;
            }

            /*Log.e("ServiceHandler", "statusCode "+ statusCode);
            Log.e("ServiceHandler", "response "+ response);*/

        } catch (Exception e) {
            response = null;
            errorMessage = e.getLocalizedMessage();
            /*e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();*/
        }

        //System.out.println("Error : \n"+errorMessage);
        return response;

    }

    public String getErrorMessage(){
        return errorMessage;
    }

}
