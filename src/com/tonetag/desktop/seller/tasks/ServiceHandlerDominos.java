package com.tonetag.desktop.seller.tasks;

import com.tonetag.desktop.seller.bean.Settings;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
//import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
//import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.ParseException;

/**
 * makes Http operations
 */
public class ServiceHandlerDominos {

    private  String response = null;
    public final static int GET = 1;
    public final static int POST = 2;

    private String errorMessage;
    private int statusCode;

    public ServiceHandlerDominos() {

    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
                                  List<NameValuePair> params) {
        //Log.e("ServiceHandler", url);
        try {
            // http client
            // set the connection timeout value to 15 seconds (15000 milliseconds)
            final HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;


            // Checking http request method type
            if (method == GET) {
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }

                try {
                    httpResponse = httpClient.execute(httpPost);
                } catch (IOException ex) {
                    response = null;
                    errorMessage = "Bad response from server";
                    //System.out.println("Exception 4 : " + ex.getLocalizedMessage());
                }
                statusCode = httpResponse.getStatusLine().getStatusCode();
                errorMessage = httpResponse.getStatusLine().getReasonPhrase();
            } else if (method == POST) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += ":" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);

                try {
                    httpResponse = httpClient.execute(httpGet);
                    if(httpResponse == null){
                        //System.out.println("httpResponse is null");
                    }else{
                        //System.out.println("httpResponse is not null");
                    }
                    
                } catch (IOException ex) {
                    httpResponse = null;
                    response = null;
                    errorMessage = "Bad response from server";
                    //System.out.println("Exception 5 : " + ex.getLocalizedMessage());
                }
                //statusCode = httpResponse.getStatusLine().getStatusCode();
                //errorMessage = httpResponse.getStatusLine().getReasonPhrase();
            }
            //if (statusCode == HttpURLConnection.HTTP_OK){
                httpEntity = httpResponse.getEntity();
                String tempResponse = "{\"success\":false}";
                try {
                    tempResponse = EntityUtils.toString(httpEntity);
                } catch (IOException ex) {
                    response = null;
                    errorMessage = "Bad response from server";
                    //System.out.println("Exception 6 : " + ex.getLocalizedMessage());
                } catch (ParseException ex) {
                    response = null;
                    errorMessage = "Bad response from server";
                    //System.out.println("Exception 7 : " + ex.getLocalizedMessage());
                }
                //System.out.println("Response : " + tempResponse);
                try {
                    Object json = new JSONParser().parse(tempResponse);
                    if ((json instanceof JSONObject) || (json instanceof JSONArray)){
                        try{
                            boolean b = (boolean)((JSONObject)json).get("success");
                            response = tempResponse;
                        }catch(Exception e){
                            response = null;
                            errorMessage = "Bad response from server";
                            //System.out.println("Exception 1 : " + e.getLocalizedMessage());
                        }
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                    response = null;
                    errorMessage = "Bad response from server";
                    //System.out.println("Exception 2 : " + e.getLocalizedMessage());
                }

            /*}else{
                response = null;
                System.out.println("statusCode != HttpURLConnection.HTTP_OK");
            }*/

            /*Log.e("ServiceHandler", "statusCode "+ statusCode);
            Log.e("ServiceHandler", "response "+ response);*/

        } catch (UnsupportedEncodingException e) {
            response = null;
            errorMessage = e.getLocalizedMessage();
            //System.out.println("Exception 3 : " + e.getLocalizedMessage());
            /*e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();*/
        }

        //System.out.println("Error : \n"+errorMessage);
        return response;

    }
    
    public String makeServiceCallForDominos(Settings settings, String url, int method,
                                  List<NameValuePair> params) {
        
        String responseString = null;
       
        try {
            // create a socket
            Socket socket = new Socket(settings.getHost(), settings.getPort());
            // perform a simple math operation “12+21”
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream()));
            
            String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += ":" + paramString;
            
            writer.write(url);
            //System.out.println("From request : "+url);
            writer.newLine();
            writer.flush();
            // get the result from the server
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            responseString = reader.readLine();
            
            reader.close();
            
            writer.close();
        }catch (Exception e) {
            responseString = null;
            errorMessage = e.getLocalizedMessage();
            //System.out.println("Exception socket : " + e.getLocalizedMessage());
        }
        
        /*if(responseString.equalsIgnoreCase("null")){
            responseString = null;
        }*/
        if(responseString != null && !responseString.contains("{\"success\"")){
            //System.out.println("OK response fails fro code : " + responseString);
            //responseString = null;
            int responseCode = 500;
            try{
                responseCode = Integer.parseInt(responseString);
            }catch(Exception e){
                //System.out.println("Exception in parsing " + e.getLocalizedMessage());
            }
            
            switch(responseCode){
                case 400:
                    errorMessage = "Bad Request";
                    break;
                case 404:
                    errorMessage = "Request Not Found";
                    break;
                case 408:
                    errorMessage = "Request Timeout";
                    break;
                case 500:
                    errorMessage = "Internal Server Error";
                    break;
                case 501:
                    errorMessage = "Not Implemented";
                    break;
                case 502:
                    errorMessage = "Bad Gateway";
                    break;
                case 504:
                    errorMessage = "Gateway Timeout";
                    break;
                case 503:
                    errorMessage = "Service Unavailable";
                    break;
                default:
                    errorMessage = "UnIdentified Error code " + responseCode;
                    break;
                    
            }
            responseString = null;
        }
        //System.out.println("From response : "+responseString);
        return responseString;
        
    }

    public String getErrorMessage(){
        return errorMessage;
    }

}
