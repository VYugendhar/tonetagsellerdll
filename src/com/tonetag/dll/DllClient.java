package com.tonetag.dll;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import net.sf.jni4net.Bridge;

public class DllClient {

	public static void loadDllLibrary() throws IOException {
//		Bridge.setVerbose(true);
		Bridge.init();
		
/*		final String myDllName = "jni4net.n." + getPlatform() +"." + getClr()+ "-" + getVersion() + ".dll";
		URL url = DllClient.class.getClassLoader().getResource("");
		String ablPath = url.toString().substring(6, url.toString().length());
		Bridge.init(new File(ablPath+"\\jni4net\\lib\\"+myDllName));*/
		
		File coreFile = new File("jni4net\\lib\\jni4net.n-0.8.8.0.dll");
		File dllFile = new File("JublFood.Payment.API.j4n.dll");
		Bridge.LoadAndRegisterAssemblyFrom(coreFile);
		Bridge.LoadAndRegisterAssemblyFrom(dllFile);

	}
	
	private static String platform;
	static String clrVersion;
	private static String version;
	
    public static synchronized String getPlatform() {
        if (platform == null) {
            String model = System.getProperty("sun.arch.data.model");
            String os = System.getProperty("os.name").toLowerCase();
            if (os.startsWith("windows")) {
                platform = "w";
            } else if (os.equals("linux")){
		platform = "l";
            } else {
System.out.println(os);
                throw new UnsupportedOperationException("Platform not supported " + os);
            }
            platform += model;
        }
        return platform;
    }
    
    public static synchronized String getClr() {
        if (clrVersion == null) {
            clrVersion = "v20";
            if (getPlatform().startsWith("w")) {
                String sysRoot = System.getenv("SystemRoot");
                if (sysRoot == null || sysRoot.equals("")) {
                    sysRoot = "c:/Windows";
                }
                File d = new File(sysRoot, "Microsoft.NET/Framework/");
                final String[] vers = d.list(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.startsWith("v4.0.");
                    }
                });
                if (vers != null && vers.length > 0) {
                    clrVersion = "v40";
                }
            }
            else if (getPlatform().startsWith("l")) {
                clrVersion = "m26";
            }
        }
        return clrVersion;
    }
    
	public static synchronized String getVersion() {
		if (version == null) {
			version = getProperty("jni4net.version");
		}
		return version;
	}
	
	public static String getProperty(String property) {
		final URL resource = DllClient.class.getClassLoader().getResource("META-INF/jni4net.properties");
		if (resource != null) {
			try {
				Properties p = new Properties();
				InputStream ins = resource.openStream();
				p.load(ins);
				ins.close();
				return p.getProperty(property, null);
			}
			catch (IOException ignore) {
			}
		}
		return null;
	}
	
	
	
	
	
	
	
}
